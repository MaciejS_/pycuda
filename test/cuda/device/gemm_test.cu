#include <gemm.cu>
//#include <utils.cu>


__global__ void test_device_gemm_single_cell(float* A, float* B, float* C,   \
                                             int wA, int hA,                 \
                                             int wB, int hB) {

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    C[threadY * wB + threadX] = device_gemm_single_cell(A, B,
                                                        wA, wB,
                                                        threadX, threadY);

}

__global__ void test_device_gemm_single_cell_with_transposedA(float* A, float* B, float* C,   \
                                                            int wA, int hA,              \
                                                            int wB, int hB) {
    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    C[threadY * wB + threadX] = device_gemm_single_cell_with_transposedA(A, B,
                                                                        wA, wB, hA,
                                                                        threadX, threadY);

}

__global__ void test_device_gemm_single_cell_with_transposedB(float* A, float* B, float* C,   \
                                                            int wA, int hA,              \
                                                            int wB, int hB) {
    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    C[threadY * hB + threadX] = device_gemm_single_cell_with_transposedB(A, B,
                                                                    wA, wB,
                                                                    threadX, threadY);

}
