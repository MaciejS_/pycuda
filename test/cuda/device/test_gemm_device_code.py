import unittest
import os

import numpy as np
import pycuda.autoinit
import pycuda.gpuarray

from pycuda.gpuarray import GPUArray

from cuda.kernels.gemm_module import GEMMModule
from cuda.kernels.kernel_module import KernelModule
from cuda.kernels import KERNELS_ROOT

TEST_KERNELS_ROOT = os.path.dirname(os.path.realpath(__file__))


class TestGEMMModule(KernelModule):
    def __init__(self):
        super().__init__(r"%s\gemm_test.cu" % TEST_KERNELS_ROOT, include_dirs=[KERNELS_ROOT])
        self._test_gemm_transposedA = self._pycuda_source_module.get_function(
            'test_device_gemm_single_cell_with_transposedA')
        self._test_gemm_transposedB = self._pycuda_source_module.get_function(
            'test_device_gemm_single_cell_with_transposedB')
        self._test_device_gemm_single_cell = self._pycuda_source_module.get_function(
            'test_device_gemm_single_cell')

    def call_test_device_gemm_single_cell(self, A, B):
        gemm_result_shape = GEMMModule.gemm_result_shape(A, B)
        C = pycuda.gpuarray.zeros(gemm_result_shape, np.float32)

        heightA, widthA = map(np.int32, A.shape)
        heightB, widthB = map(np.int32, B.shape)

        # block size dimensions order: (X, Y, Z), numpy array holds (Y, X, Z)
        cuda_block_dim = KernelModule.block_dim_for_ndarray_shape(gemm_result_shape)

        self._test_device_gemm_single_cell(A, B, C,
                                            widthA, heightA,
                                            widthB, heightB,
                                            block=cuda_block_dim)

        return C

    def call_test_gemm_transposedA(self, A, B) -> GPUArray:
        gemm_result_shape = GEMMModule.gemm_result_shape(A.T, B)  # A.T is required here to get the correct resulting shape size
        C = pycuda.gpuarray.zeros(gemm_result_shape, np.float32)

        heightA, widthA = map(np.int32, A.shape)
        heightB, widthB = map(np.int32, B.shape)

        # block size dimensions order: (X, Y, Z), numpy array holds (Y, X, Z)
        cuda_block_dim = KernelModule.block_dim_for_ndarray_shape(gemm_result_shape)

        self._test_gemm_transposedA(A, B, C,
                                    widthA, heightA,
                                    widthB, heightB,
                                    block=cuda_block_dim)

        return C.T

    def call_test_gemm_transposedB(self, A, B) -> GPUArray:
        gemm_result_shape = GEMMModule.gemm_result_shape(A, B.T)  # B.T is required here to get the correct resulting shape size
        C = pycuda.gpuarray.zeros(gemm_result_shape, np.float32)

        heightA, widthA = map(np.int32, A.shape)
        heightB, widthB = map(np.int32, B.shape)

        # block size dimensions order: (X, Y, Z), numpy array holds (Y, X, Z)
        cuda_block_dim = KernelModule.block_dim_for_ndarray_shape(gemm_result_shape)

        self._test_gemm_transposedB(A, B, C,
                                    widthA, heightA,
                                    widthB, heightB,
                                    block=cuda_block_dim)

        return C.T


class TestDeviceCode(unittest.TestCase):
    def setUp(self):
        self.gemm_test_module = TestGEMMModule()

        self.cpu_a = np.random.rand(3, 5)
        self.cpu_b = np.random.rand(3, 10)
        # self.cpu_a = np.arange(12).reshape(3, 4)
        # self.cpu_b = np.arange(30).reshape(3, 10)

        self.gpu_a, self.gpu_b = [x.astype(np.float32) for x in map(pycuda.gpuarray.to_gpu, (self.cpu_a, self.cpu_b))]

    def test_device_gemm_single_cell(self):
        a_t = self.cpu_a.T
        cpu_result = a_t.dot(self.cpu_b)

        gpu_a_t = pycuda.gpuarray.to_gpu(a_t.copy()).astype(np.float32)
        gpu_result = self.gemm_test_module.call_test_device_gemm_single_cell(gpu_a_t, self.gpu_b).get()

        self.assertFalse(np.allclose(gpu_a_t.get(), np.arange(15).reshape(5, 3)),
                         msg="Known PyCUDA issue, untransposed, underlying 1D array is copied instead of transposed numpy view")
        self.assertTrue(np.allclose(gpu_result, cpu_result),
                        msg=(gpu_result, cpu_result, gpu_result == cpu_result))

    def test_device_transposedA_gemm_function(self):
        cpu_result = self.cpu_b.T.dot(self.cpu_a).T
        gpu_result = self.gemm_test_module.call_test_gemm_transposedA(self.gpu_b, self.gpu_a).get()

        self.assertTrue(np.allclose(gpu_result, cpu_result), msg=(gpu_result, cpu_result))

    def test_device_transposedB_gemm_function(self):
        input_sample = self.cpu_a.T.dot(self.cpu_b)
        gpu_input_sample = pycuda.gpuarray.to_gpu(input_sample).astype(np.float32)
        result = self.gemm_test_module.call_test_gemm_transposedB(gpu_input_sample,
                                                                  self.gpu_b)

        cpu_result = input_sample.dot(self.cpu_b.T).T
        gpu_result = result.get()
        self.assertTrue(np.allclose(gpu_result, cpu_result), msg=(gpu_result, cpu_result))
