import unittest
import numpy as np

from cuda.kernels.kernel_module import KernelModule


class TestCUDABlockDim(unittest.TestCase):

    def test_raises_when_1d_array_is_empty(self):
        numpy_array = np.random.rand(0)
        with self.assertRaises(ValueError):
            KernelModule.block_dim_for_ndarray_shape(numpy_array.shape)

    def test_raises_when_2d_array_is_empty(self):
        numpy_array = np.random.rand(0,0)
        with self.assertRaises(ValueError):
            KernelModule.block_dim_for_ndarray_shape(numpy_array.shape)

    def test_for_1d_array(self):
        x = 4
        numpy_array = np.random.rand(x)
        self.assertTupleEqual(KernelModule.block_dim_for_ndarray_shape(numpy_array.shape), (x, 1, 1))

    def test_for_2d_array(self):
        x, y = 4, 5
        numpy_array = np.random.rand(y, x)
        self.assertTupleEqual(KernelModule.block_dim_for_ndarray_shape(numpy_array.shape), (x, y, 1))

    def test_for_3d_array(self):
        x, y, z = 4, 5, 2
        numpy_array = np.random.rand(y, x, z)
        self.assertTupleEqual(KernelModule.block_dim_for_ndarray_shape(numpy_array.shape), (x, y, z))

    def test_raise_for_4d_array_and_higher(self):
        x, y, z, n = 4, 5, 2, 3
        numpy_array = np.random.rand(y, x, z, n)
        with self.assertRaises(ValueError):
            KernelModule.block_dim_for_ndarray_shape(numpy_array.shape)


if __name__ == '__main__':
    unittest.main()
