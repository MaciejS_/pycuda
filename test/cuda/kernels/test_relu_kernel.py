import unittest
from parameterized import parameterized, param

import numpy as np
import pycuda
import pycuda.driver
import pycuda.compiler
import pycuda.autoinit
import pycuda.gpuarray

from cuda.kernels import KERNELS_ROOT

from cuda.kernels.relu_kernel import ReLUKernel

class TestApply(unittest.TestCase):
    input_vector = pycuda.gpuarray.to_gpu(np.array([-1.0, 0, 1.0], dtype=np.float32))
    expected_output_vector = pycuda.gpuarray.to_gpu(np.array([0, 0, 1.0], dtype=np.float32))

    input_array = pycuda.gpuarray.to_gpu(np.array([[-1.0, 0, 1.0],
                                                   [0, 1.0, -1.0]],
                                                  dtype=np.float32))

    expected_output_array = pycuda.gpuarray.to_gpu(np.array([[0, 0, 1.0],
                                                             [0, 1.0, 0]],
                                                            dtype=np.float32))

    def setUp(self):
        self.relu_kernel = ReLUKernel()
        pass

    @parameterized.expand([
        ("vector", input_vector, expected_output_vector),
        ("array", input_array, expected_output_array)
        ])
    def test_apply_works_with_prepared_output(self, _, input_data, expected_output):
        gpu_output_array = pycuda.gpuarray.zeros_like(input_data)

        self.relu_kernel.apply(input_data, gpu_output_array)

        self.assertTrue(np.allclose(gpu_output_array.get(), expected_output.get()),
                        msg=str(gpu_output_array.get()) + "\n" + str(expected_output.get()))

    @parameterized.expand([
        ("vector", input_vector, expected_output_vector),
        ("array", input_array, expected_output_array)
        ])
    def test_apply_works_without_prepared_output(self, _, input_data, expected_output):
        gpu_output_array = self.relu_kernel.apply(input_data)

        self.assertTrue(np.allclose(gpu_output_array.get(), expected_output.get()),
                        msg=str(gpu_output_array.get()) + "\n" + str(expected_output.get()))

if __name__ == '__main__':
    unittest.main()
