import unittest

import numpy as np

import pycuda.autoinit
import pycuda.gpuarray
from parameterized import parameterized

from cuda.kernels.nn_kernel import NNKernel
from nn.neural_network.activation_functions.relu import ReLU
from nn.neural_network.network.learning_network import LearningNumpyNN
from nn.neural_network.network.network import NumpyNN

NN_KERNEL = NNKernel() # NNKernel is instantiated only once and therefore code compilation occurs only once

class TestFeeding(unittest.TestCase):
    def setUp(self):
        self.cpu_nn = NumpyNN.from_size_data(3,
                                             (4, 2),
                                             activation_functions=(ReLU.apply_in_place,) * 2)
        self.nn_kernel = NN_KERNEL

    @parameterized.expand([
        ("works_with_vector", np.random.rand(1, 3)),
        ("works_with_matrix", np.random.rand(5, 3))
        ])
    def test_feed_matrix_gpuarray(self, _, input_matrix):
        weight_matrices = self.cpu_nn.weight_matrices
        gpu_weight_matrices = list(map(pycuda.gpuarray.to_gpu, weight_matrices))

        gpu_input_matrix = pycuda.gpuarray.to_gpu(input_matrix)
        output = self.nn_kernel.feed_gpuarray(gpu_input_matrix,
                                              gpu_weight_matrices)

        cpu_output = self.cpu_nn.feed(input_matrix)
        gpu_output = output.get()
        self.assertTrue(np.allclose(gpu_output, cpu_output),
                        msg=(gpu_output, cpu_output))


class TestIntermediateMatrices(unittest.TestCase):

    def setUp(self):
        self.cpu_nn = LearningNumpyNN.from_size_data(3,
                                             (4, 2),
                                             activation_functions=None)
        self.nn_kernel = NN_KERNEL

    def test_intermediate_matrices_contents(self):
        input_matrix = np.random.rand(3)
        weight_matrices = self.cpu_nn.weight_matrices
        gpu_weight_matrices = list(map(pycuda.gpuarray.to_gpu, weight_matrices))

        gpu_input_matrix = pycuda.gpuarray.to_gpu(input_matrix)

        gpu_raw_input_matrices = self.nn_kernel.prepare_raw_input_matrices(gpu_input_matrix, weight_matrices)
        gpu_weighted_input_matrices = self.nn_kernel.prepare_weighted_input_matrices(gpu_input_matrix, weight_matrices)

        output = self.nn_kernel.feed_gpuarray(gpu_input_matrix,
                                              gpu_weight_matrices,
                                              gpu_raw_input_matrices,
                                              gpu_weighted_input_matrices)

        cpu_output = self.cpu_nn.feed(input_matrix)

        cpu_raw_inputs = self.cpu_nn._raw_inputs
        cpu_weighted_inputs = self.cpu_nn._weighted_inputs

        gpu_output = output.get()
        self.assertTrue(np.allclose(gpu_output, cpu_output),
                        msg=(gpu_output, cpu_output))
        
        for gpu_raw_inputs, cpu_raw_inputs in zip(gpu_raw_input_matrices, cpu_raw_inputs):
            self.assertTrue(np.allclose(gpu_raw_inputs.get(), cpu_raw_inputs))
            
        for gpu_weighted_inputs, cpu_weighted_inputs in zip(gpu_weighted_input_matrices, cpu_weighted_inputs):
            self.assertTrue(np.allclose(gpu_weighted_inputs.get(), cpu_weighted_inputs))


class TestBackprop(unittest.TestCase):
    def setUp(self):

        self.learning_rate = 0.03
        # NN is initialized with predefined matrices to ensure determinism of results
        self.teachable_weight_matrices = (np.asarray([[0.83510995, 0.0512483, 0.08957317, 0.89735866],
                                                      [0.0335088, -0.13946636, 0.5856443, -0.51295179]]),
                                          np.asarray([[0.41522264],
                                                      [0.00924876],
                                                      [-0.58161056],
                                                      [0.89126498]]))
        self.lnn = LearningNumpyNN.from_weight_matrices(self.teachable_weight_matrices,
                                                        learning_rate=self.learning_rate)

        self.nn_kernel = NN_KERNEL

    def test_kernel_backprop_performs_the_same_update_to_matrices(self):
        examples = list(map(np.asarray, [(0, 0),
                                         (0, 1),
                                         (1, 0),
                                         (1, 1)]))
        expected = list(map(np.asarray, [0, 1, 1, 0]))
        error = 1.e-2

        selected_example = 2
        initial_prediction = self.lnn.feed(examples[selected_example])
        error_vector = expected[selected_example] - initial_prediction

        gpu_error_vector = pycuda.gpuarray.to_gpu(error_vector.astype(np.float32))
        gpu_weighted_inputs = list(map(pycuda.gpuarray.to_gpu, [x.astype(np.float32) for x in self.lnn._weighted_inputs]))
        gpu_raw_inputs = list(map(pycuda.gpuarray.to_gpu, [x.astype(np.float32) for x in self.lnn._raw_inputs]))
        gpu_weight_matrices = list(map(pycuda.gpuarray.to_gpu, [x.astype(np.float32) for x in self.lnn.weight_matrices]))

        foo = self.nn_kernel.backpropagate_vector(gpu_error_vector,
                                            gpu_weighted_inputs,
                                            gpu_raw_inputs,
                                            gpu_weight_matrices,
                                            learning_rate=self.learning_rate)

        self.lnn.backpropagate(error_vector)

        updated_weight_matrices = self.lnn.weight_matrices
        updated_kernel_weight_matrices = [x.get() for x in foo]

        for updated_cpu, updated_gpu, original in zip(updated_weight_matrices,
                                                      updated_kernel_weight_matrices,
                                                        self.teachable_weight_matrices):
            self.assertFalse(np.allclose(updated_gpu, original), msg=(updated_gpu, original))
            self.assertFalse(np.allclose(updated_cpu, original), msg=(updated_cpu, original))
            self.assertTrue(np.allclose(updated_cpu, updated_gpu), msg=(updated_weight_matrices, updated_kernel_weight_matrices))


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.nn_kernel = NN_KERNEL

    @parameterized.expand([
        (1, (3,5), (8,2), (5,1,1)),
        (10, (3,5), (8,2), (5,10,1))
        ])
    def test_get_sufficient_block_sizes(self, input_matrix_y, a_shape, b_shape, expected):
        input_matrix = np.random.rand(input_matrix_y, 5)
        a = np.random.rand(*a_shape)
        b = np.random.rand(*b_shape)

        block_size = NNKernel.get_sufficient_block_size_for_forward_prop(input_matrix, [a, b])

        self.assertTupleEqual(block_size, expected)



if __name__ == '__main__':
    unittest.main()
