import unittest
import logging
import numpy as np
import pycuda
import pycuda.driver
import pycuda.compiler
import pycuda.gpuarray

import pycuda.autoinit
from parameterized import parameterized

from cuda.kernels.gemm_module import GEMMModule
from cuda.kernels import KERNELS_ROOT


class TestGEMMModule(unittest.TestCase):

    def setUp(self):
        self.gemm_kernel = GEMMModule()
        logging.basicConfig()

    @parameterized.expand([
        ("regular_matrices", (3,5), (5,4)),
        ("vector_and_matrix", (1, 5), (5,4)),
        ("matrix_and_vector", (5,4), (4, 1))
        ])
    def test_gemm_result_shape_function(self, _, a_shape, b_shape):
        a = np.zeros(a_shape)
        b = np.zeros(b_shape)

        actual_gemm_result = a.dot(b)
        calculated_gemm_result_shape = self.gemm_kernel.gemm_result_shape(a,b)

        self.assertTupleEqual(calculated_gemm_result_shape, actual_gemm_result.shape)

    def test_gemm_result_shape_function_raises_on_incompatible_shapes(self):
        a = np.zeros((3,2))
        b = np.zeros((5,4))

        with self.assertRaises(ValueError):
            self.gemm_kernel.gemm_result_shape(a,b)

class TestNDArrayGEMM(unittest.TestCase):

    def setUp(self):
        self.gemm_kernel = GEMMModule()
        logging.basicConfig()

    def test_gemm_identity_matrix(self):
        array_dimensions = {"a": (3, 5),
                            "b": (5, 5)}

        host_a = np.arange(1, 16).reshape(*array_dimensions['a']).astype(np.float32)
        host_b = np.eye(*array_dimensions['b']).astype(np.float32)

        host_c = self.gemm_kernel.ndarray_gemm(host_a, host_b)
        cpu_c = np.dot(host_a, host_b)

        self.assertTupleEqual(host_c.shape, cpu_c.shape, msg="Shapes don't match")
        self.assertTrue(np.allclose(host_c, cpu_c))

    def test_gemm_random_matrix(self):
        array_dimensions = {"a": (3, 5),
                            "b": (5, 4)}

        host_a = np.random.rand(*array_dimensions['a']).astype(np.float32)
        host_b = np.random.rand(*array_dimensions['b']).astype(np.float32)

        host_c = self.gemm_kernel.ndarray_gemm(host_a, host_b)
        cpu_c = np.dot(host_a, host_b)

        self.assertTupleEqual(host_c.shape, cpu_c.shape, msg="Shapes don't match")
        self.assertTrue(np.allclose(host_c, cpu_c))


class TestGPUArrayGEMM(unittest.TestCase):

    def setUp(self):
        self.gemm_kernel = GEMMModule()
        logging.basicConfig()

    def test_gemm_identity_matrix(self):
        array_dimensions = {"a": (3, 5),
                            "b": (5, 5)}

        host_a = np.arange(1, 16).reshape(*array_dimensions['a']).astype(np.float32)
        host_b = np.eye(*array_dimensions['b']).astype(np.float32)

        gpu_a, gpu_b = map(pycuda.gpuarray.to_gpu, (host_a, host_b))

        device_c = self.gemm_kernel.gpuarray_gemm(gpu_a, gpu_b)
        cpu_c = np.dot(host_a, host_b)

        self.assertTupleEqual(device_c.shape, cpu_c.shape, msg="Shapes don't match")
        self.assertTrue(np.allclose(device_c.get(), cpu_c))

    @parameterized.expand([
        ("matrix_and_matrix", (3,5), (5,4)),
        ("vector_and_matrix", (1,5), (5,4)),
        ("matrix_and_matrix", (3,5), (5,1))
        ])
    def test_gemm_random(self, _, a_shape, b_shape):
        host_a = np.random.rand(*a_shape).astype(np.float32)
        host_b = np.random.rand(*b_shape).astype(np.float32)

        gpu_a, gpu_b = map(pycuda.gpuarray.to_gpu, (host_a, host_b))

        device_c = self.gemm_kernel.gpuarray_gemm(gpu_a, gpu_b)
        cpu_c = np.dot(host_a, host_b)

        self.assertTupleEqual(device_c.shape, cpu_c.shape, msg="Shapes don't match")
        self.assertTrue(np.allclose(device_c.get(), cpu_c))


class TestRawGEMMCode(unittest.TestCase):

    def setUp(self):

        GEMM_MODULE = r"%s\gemm.cu" % KERNELS_ROOT

        with open(GEMM_MODULE) as gemm_module_file:
            kernel_code = gemm_module_file.read()
            self.gemm_module = pycuda.compiler.SourceModule(kernel_code)
            self.simple_gemm_kernel = self.gemm_module.get_function("simple_gemm")

    def test_gemm_on_a_small_matrix(self):
        array_dimensions = {"a": (3, 5),
                            "b": (5, 4)}
        host_a, host_b = [np.random.rand(*array_dimensions[x]).astype(np.float32) for x in ('a', 'b')]

        resulting_dimensions = (array_dimensions['a'][0], array_dimensions['b'][-1])

        device_a, device_b = map(pycuda.gpuarray.to_gpu, (host_a, host_b))
        device_c = pycuda.gpuarray.to_gpu(np.zeros(resulting_dimensions).astype(np.float32))

        widthA, heightA = map(np.int32, array_dimensions['a'][::-1])
        widthB, heightB = map(np.int32, array_dimensions['b'][::-1])

        cuda_block_dim = resulting_dimensions[::-1] + (1,)

        self.simple_gemm_kernel(device_a, device_b, device_c,
                                widthA, heightA,
                                widthB, heightB,
                                block=cuda_block_dim)

        cpu_c = np.dot(host_a, host_b)

        print(cpu_c)
        print(device_c.get())

        self.assertTrue(np.allclose(device_c.get(), cpu_c))

    def test_gemm_on_identity_matrix(self):
        array_dimensions = {"a": (3, 5),
                            "b": (5, 5)}
        # host_a, host_b = [np.random.rand(*array_dimensions[x]).astype(np.float32) for x in ('a', 'b')]

        host_a = np.arange(1, 16).reshape(*array_dimensions['a']).astype(np.float32)
        host_b = np.eye(*array_dimensions['b']).astype(np.float32)

        resulting_dimensions = array_dimensions['a']

        device_a, device_b = map(pycuda.gpuarray.to_gpu, (host_a, host_b))
        device_c = pycuda.gpuarray.to_gpu(np.zeros(resulting_dimensions).astype(np.float32))

        widthA, heightA = map(np.int32, array_dimensions['a'][::-1])
        widthB, heightB = map(np.int32, array_dimensions['b'][::-1])

        cuda_block_dim = resulting_dimensions[::-1] + (1,)

        self.simple_gemm_kernel(device_a, device_b, device_c,
                                widthA, heightA,
                                widthB, heightB,
                                block=cuda_block_dim)

        cpu_c = np.dot(host_a, host_b)

        print(cpu_c)
        print(device_c.get())

        self.assertTrue(np.allclose(device_c.get(), cpu_c))

if __name__ == '__main__':
    unittest.main()
