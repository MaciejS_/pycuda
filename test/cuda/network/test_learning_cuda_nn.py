import unittest

import numpy as np

from cuda.neural_network.learning_cuda_nn import MonolithicLearningCUDANN, ModularLearningCUDANN

import pycuda.autoinit
import pycuda.gpuarray


class TestMonolithicLearningNN(unittest.TestCase):
    def setUp(self):
        # NN is initialized with predefined matrices to ensure determinism of results
        self.lnn = MonolithicLearningCUDANN.from_weight_matrices(
            (np.asarray([[0.83510995, 0.0512483, 0.08957317, 0.89735866],
                         [0.0335088, -0.13946636, 0.5856443, -0.51295179]]),
             np.asarray([[0.41522264],
                         [0.00924876],
                         [-0.58161056],
                         [0.89126498]])),
            learning_rate=0.03)

    def test_network_learns_xor(self):
        gpu_examples = [x.astype(np.float32)
                        for x
                        in map(pycuda.gpuarray.to_gpu,
                               map(np.asarray, [(0, 0),
                                                (0, 1),
                                                (1, 0),
                                                (1, 1)]))]
        expected = list(map(np.atleast_2d, map(np.asarray, [0, 1, 1, 0])))
        expected_comparable = np.atleast_2d(np.asarray([0, 1, 1, 0])).T
        gpu_expected = [x.astype(np.float32)
                        for x
                        in map(pycuda.gpuarray.to_gpu, expected)]
        error = 1.e-3

        initial_predictions = np.concatenate([x.get() for x in map(self.lnn.feed, gpu_examples)])

        self.assertFalse(
            np.allclose(initial_predictions, expected, rtol=error, atol=error))  # ensure that NN is not trained

        results_good_enough = False
        for x in range(1000):
            self.lnn.learnOn(gpu_examples, gpu_expected)
            final_predictions = np.concatenate([x.get() for x in map(self.lnn.feed, gpu_examples)])
            if np.allclose(final_predictions, expected_comparable, rtol=error, atol=error):
                results_good_enough = True
                break

        self.assertTrue(results_good_enough, msg=(final_predictions, expected_comparable))


class TestModularLearningCUDANN(TestMonolithicLearningNN):
    def setUp(self):
        # NN is initialized with predefined matrices to ensure determinism of results
        self.lnn = ModularLearningCUDANN.from_weight_matrices(
            (np.asarray([[0.83510995, 0.0512483, 0.08957317, 0.89735866],
                         [0.0335088, -0.13946636, 0.5856443, -0.51295179]]),
             np.asarray([[0.41522264],
                         [0.00924876],
                         [-0.58161056],
                         [0.89126498]])),
            learning_rate=0.03)


if __name__ == '__main__':
    unittest.main()
