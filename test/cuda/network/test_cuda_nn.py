import logging
import unittest

import numpy as np

import pycuda.autoinit
from cuda.kernels.relu_kernel import ReLUKernel
from cuda.neural_network.cuda_nn import ModularCUDANN
from cuda.neural_network.cuda_nn import MonolithicCUDANN
from nn.neural_network.network.network import NumpyNN

class TestCudaNN(unittest.TestCase):

    def setUp(self):
        logging.basicConfig()

        self.input_layer_size = 10
        self.hidden_layer_size = 5
        self.output_layer_size = 2

        self.layer_sizes = (self.input_layer_size, self.hidden_layer_size, self.output_layer_size)

        self.cpu_nn = NumpyNN.from_size_data(self.layer_sizes[0], self.layer_sizes[1:], (self.cpu_relu, )*(len(self.layer_sizes)-1))
        self.cuda_nn = ModularCUDANN(self.layer_sizes[0], self.layer_sizes[1:], self.cpu_nn.weight_matrices)

        input_vector = np.random.rand(self.input_layer_size)

        self.cuda_nn_output = self.cuda_nn.feed(input_vector)
        self.cpu_nn_output = self.cpu_nn.feed(input_vector)

        # numpy never keeps empty dimensions (GPU GEMM does), so we need to scale the vector up
        self.cpu_nn_output = np.atleast_2d(self.cpu_nn_output)

    @staticmethod
    def cpu_relu(arr):
        arr[arr < 0] = 0
        return arr

    def test_cuda_nn_feed_result(self):
        self.assertTrue(np.allclose(self.cuda_nn_output, self.cpu_nn_output),
                        msg=(self.cuda_nn_output, self.cpu_nn_output))

    def test_gemm_result_sizes_match(self):

        self.assertTupleEqual(self.cuda_nn_output.shape, self.cpu_nn_output.shape)

    def test_assert_both_nns_have_the_same_weight_matrices(self):

        for cuda_weight_matrix, cpu_weight_matrix in zip(self.cuda_nn.weight_matrices, self.cpu_nn.weight_matrices):
            self.assertTrue(np.allclose(cuda_weight_matrix, cpu_weight_matrix))

    def test_assert_both_nns_use_relu(self):

        import pycuda.gpuarray

        sample_input = np.random.rand(5,3).astype(np.float32)
        expected_output = self.cpu_relu(sample_input)

        cpu_nn_activation_functions = self.cpu_nn.activation_functions

        relu_kernel = self.cuda_nn.relu_kernel

        for activation_function in cpu_nn_activation_functions:
            cpu_result = activation_function(sample_input)
            cuda_result = relu_kernel.apply(pycuda.gpuarray.to_gpu(sample_input))

            self.assertTrue(np.allclose(cpu_result, expected_output))
            self.assertTrue(np.allclose(cuda_result.get(), expected_output))

class TestMonolithicCudaNN(unittest.TestCase):

    def setUp(self):
        logging.basicConfig()

        self.input_layer_size = 10
        self.hidden_layer_size = 5
        self.output_layer_size = 2

        self.layer_sizes = (self.input_layer_size, self.hidden_layer_size, self.output_layer_size)

        self.cpu_nn = NumpyNN.from_size_data(self.layer_sizes[0], self.layer_sizes[1:],
                                             (self.cpu_relu,) * (len(self.layer_sizes) - 1))

        self.cuda_nn = MonolithicCUDANN(self.layer_sizes[0], self.layer_sizes[1:], self.cpu_nn.weight_matrices)

        input_vector = np.random.rand(self.input_layer_size)

        self.cuda_nn_output = self.cuda_nn.feed(input_vector)
        self.cpu_nn_output = self.cpu_nn.feed(input_vector)

        # numpy never keeps empty dimensions (GPU GEMM does), so we need to scale the vector up
        self.cpu_nn_output = np.atleast_2d(self.cpu_nn_output)

    @staticmethod
    def cpu_relu(arr):
        arr[arr < 0] = 0
        return arr

    def test_cuda_nn_feed_result(self):
        self.assertTrue(np.allclose(self.cuda_nn_output, self.cpu_nn_output),
                        msg=(self.cuda_nn_output, self.cpu_nn_output))


if __name__ == '__main__':
    unittest.main()
