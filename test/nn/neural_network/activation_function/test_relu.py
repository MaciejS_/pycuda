import unittest
import numpy as np

from parameterized import parameterized

from nn.neural_network.activation_functions.relu import ReLU


class TestReLUActivationFunction(unittest.TestCase):
    input_vector = np.array([-1.0, 0, 1.0, 2.0])
    expected_output_vector = np.array([0, 0, 1.0, 2.0])
    expected_derivative_vector = np.array([0, 0, 1.0, 1.0])

    input_array = np.array([[-1.0, 0, 1.0, 2.0],
                            [0, 1.0, -1.0, 2.0]])
    expected_output_array = np.array([[0, 0, 1.0, 2.0],
                                      [0, 1.0, 0, 2.0]])
    expected_derivative_array = np.array([[0, 0, 1.0, 1.0],
                                          [0, 1.0, 0, 1.0]])

    def setUp(self):
        self.relu = ReLU()

    @parameterized.expand([
        ("vector", np.copy(input_vector), np.copy(expected_output_vector)),
        ("array", np.copy(input_array), np.copy(expected_output_array))
        ])
    def test_relu(self, _, values, expected_output):
        initial_values = np.copy(values)
        relu_result = self.relu.apply(values)
        self.assertTrue(np.allclose(relu_result, expected_output), msg=(relu_result, expected_output))
        self.assertTrue(np.allclose(values, initial_values))

    @parameterized.expand([
        ("vector", np.copy(input_vector), np.copy(expected_output_vector)),
        ("array", np.copy(input_array), np.copy(expected_output_array))
        ])
    def test_inplace_relu(self, _, values, expected_output):
        self.relu.apply_in_place(values)
        self.assertTrue(np.allclose(values, expected_output),
                        msg=(values, expected_output))

    @parameterized.expand([
        ("vector", np.copy(input_vector), np.copy(expected_derivative_vector)),
        ("array", np.copy(input_array), np.copy(expected_derivative_array))
        ])
    def test_derivative(self, _, values, expected_output):
        initial_values = np.copy(values)
        self.assertTrue(np.allclose(self.relu.apply_derivative(values), expected_output), msg=values)
        self.assertTrue(np.allclose(values, initial_values))

    @parameterized.expand([
        ("vector", np.copy(input_vector), np.copy(expected_derivative_vector)),
        ("array", np.copy(input_array), np.copy(expected_derivative_array))
        ])
    def test_inplace_derivative(self, _, values, expected_output):
        self.relu.apply_derivative_in_place(values)
        self.assertTrue(np.allclose(values, expected_output), msg=values)


if __name__ == '__main__':
    unittest.main()
