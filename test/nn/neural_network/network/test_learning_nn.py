import unittest
from nn.neural_network.network.learning_network import LearningNumpyNN
import numpy as np


class TestLearning(unittest.TestCase):
    def setUp(self):
        # NN is initialized with predefined matrices to ensure determinism of results
        self.lnn = LearningNumpyNN.from_weight_matrices((np.asarray([[0.83510995, 0.0512483, 0.08957317, 0.89735866],
                                                                     [0.0335088, -0.13946636, 0.5856443, -0.51295179]]),
                                                         np.asarray([[0.41522264],
                                                                     [0.00924876],
                                                                     [-0.58161056],
                                                                     [0.89126498]])),
                                                         learning_rate=0.03)

    def test_network_learns_xor(self):
        examples = list(map(np.asarray, [(0, 0),
                                         (0, 1),
                                         (1, 0),
                                         (1, 1)]))
        expected = list(map(np.asarray, [0, 1, 1, 0]))
        error = 1.e-2

        initial_predictions = np.concatenate(list(map(self.lnn.feed, examples)))

        self.assertFalse(np.allclose(initial_predictions, expected, rtol=error, atol=error))  # ensure that NN is not trained

        results_good_enough = False
        for x in range(1000):
            self.lnn.learnOn(examples, expected)
            final_predictions = np.concatenate(list(map(self.lnn.feed, examples)))
            if np.allclose(final_predictions, expected, rtol=error, atol=error):
                results_good_enough = True
                break

        self.assertTrue(results_good_enough, msg=final_predictions)


class TestConstructor(unittest.TestCase):
    def test_raises_on_attempt_to_set_custom_activation_functions(self):
        with self.assertRaises(NotImplementedError):
            lnn = LearningNumpyNN.from_size_data(3, (4, 2), activation_functions=(lambda x: x,)*2)

if __name__ == '__main__':
    unittest.main()
