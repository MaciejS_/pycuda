import os
import pickle
import random
import unittest
from copy import deepcopy

import scipy.special
from nn.neural_network.network.network import *


class TestNNConstructors(object):
    def test_nn_creation_from_size_data(self):
        inputs = random.randint(1, 10)
        layer_sizes = tuple([random.randint(1, 10) for _ in range(random.randint(1, 10))])
        network = self.tested_network_class.from_size_data(inputs, layer_sizes)
        weight_matrices = network.weight_matrices

        self.assertEqual(network.inputs, inputs)
        ext_layer_sizes = (inputs,) + layer_sizes
        shapes = zip(ext_layer_sizes, ext_layer_sizes[1:])
        for shape, weight_matrix in zip(shapes, weight_matrices):
            self.assertTupleEqual(shape, weight_matrix.shape)
        pass

    def test_nn_creation_from_weight_matrices(self):
        inputs = random.randint(1, 10)
        layers = tuple([random.randint(1, 10) for i in range(random.randint(1, 10))])
        weight_matrices = self.tested_network_class.generate_random_weights(inputs, layer_sizes=layers)

        network = self.tested_network_class.from_weight_matrices(weight_matrices)
        self.assertEqual(network.inputs, inputs)
        ext_layer_sizes = (inputs,) + layers
        shapes = zip(ext_layer_sizes, ext_layer_sizes[1:])
        for shape, weight_matrix in zip(shapes, weight_matrices):
            self.assertTupleEqual(shape, weight_matrix.shape)

    def test_nn_creation_with_invalid_arguments(self):
        for value in (-random.randint(0, 10), 0):
            self.assertRaises(ValueError, lambda: self.tested_network_class.from_size_data(value, (1,)))
            self.assertRaises(ValueError, lambda: self.tested_network_class.from_size_data(1, (value,)))


class TestNumpyNNConstructors(TestNNConstructors):
    def setUp(self):
        self.tested_network_class = NumpyNN


class GenericNNTest(object):
    def setUp(self):
        self.tested_network_class = NeuralNetwork
        self.stacking_factor = random.randint(2, 200)
        self.nn_inputs = random.randint(3, 20)
        self.nn_layers = random.randint(3, 10)
        self.nn_layer_sizes = np.random.randint(3, 20, self.nn_layers)
        self.weight_matrices = NeuralNetwork.generate_random_weights(self.nn_inputs,
                                                                     self.nn_layer_sizes)
        self.nn_input_vector = NeuralNetwork._generate_weights_matrix(self.nn_inputs,
                                                                      1)
        self.nn_input_matrix = NeuralNetwork._generate_weights_matrix(self.nn_inputs,
                                                                      self.stacking_factor)


class GenericNumpyNNTest(GenericNNTest):
    def setUp(self):
        GenericNNTest.setUp(self)
        self.tested_network_class = NumpyNN
        self.nn = self.tested_network_class.from_weight_matrices(self.weight_matrices)
        self.other_nn = self.tested_network_class.from_size_data(self.nn_inputs + 3, self.nn_layer_sizes[1:])


class TestWeightsManipulationBehavior(GenericNNTest):
    def setUp(self):
        # prepares a sample network
        self.tested_network_class = NumpyNN
        self.inputs = random.randint(1, 10)
        self.layers = tuple([random.randint(1, 10) for i in range(random.randint(1, 10))])
        self.network = self.tested_network_class.from_size_data(self.inputs, self.layers)

    def test_weight_matrices_swapping(self):
        new_matrices = self.tested_network_class.generate_random_weights(self.network.inputs,
                                                                         self.network.layer_sizes)
        self.network.weight_matrices = new_matrices

        self.assertTrue(all(np.allclose(new_matrix, inserted_matrix)
                            for new_matrix, inserted_matrix
                            in zip(new_matrices, self.network.weight_matrices)))

    def test_attempt_to_inject_a_malformed_weights_matrix(self):
        injection_point = random.randint(0, len(self.layers) - 1)
        valid_matrix = self.network.weight_matrices[injection_point]
        invalid_shape = (valid_matrix.shape[0] + 1, valid_matrix.shape[1] + 1)
        invalid_matrix = np.random.rand(*invalid_shape)

        # tuple shouldn't support item assignment (protects 'getted' data from mishandling)
        with self.assertRaises(TypeError):
            # try to replace a single weight matrix, this stateement should raise
            self.network.weight_matrices[injection_point] = invalid_matrix

        # assert that previous assignment didn't have any effect
        self.assertTrue(np.allclose(self.network.weight_matrices[injection_point], valid_matrix))


class TestWeightsManipulationBehaviourForNumpyNN(TestWeightsManipulationBehavior):
    def setUp(self):
        TestWeightsManipulationBehavior.setUp(self)
        self.tested_network_class = NumpyNN


class TestMatrixRaveling(unittest.TestCase):
    def setUp(self):
        self.tested_network_class = NumpyNN
        self.nn = self.tested_network_class.from_size_data(random.randint(1, 10), (5, 3))
        self.weight_matrices = self.nn.weight_matrices
        self.raveled_matrices = self.nn.ravel_weight_matrices(self.weight_matrices)

    def test_raveled_matrices_shape(self):
        expected_raveled_length = sum(np.prod(weight_matrix.shape) for weight_matrix in self.weight_matrices)
        self.assertEqual(expected_raveled_length, len(self.raveled_matrices))

    def test_raveled_matrices_contents(self):
        caret = 0
        for matrix in self.weight_matrices:
            for row in matrix:
                self.assertTrue(np.all(row == np.asarray(self.raveled_matrices[caret:caret + len(row)])))
                caret += len(row)

    def test_unraveling_correctness(self):
        initial_matrices = self.nn.weight_matrices

        weights_vector = NeuralNetwork.ravel_weight_matrices(initial_matrices)
        self.assertTrue(len(weights_vector.shape) == 1)

        matrices = self.nn.unravel_weights_vector(weights_vector)
        for m1, m2 in zip(initial_matrices, matrices):
            self.assertTrue((m1 == m2).all())


#
# class TestCustomNNGetterSetterProperties(GenericNNTest):
#     def setUp(self):
#         GenericNNTest.setUp(self)
#
#     def test_weight_matrices_setting(self):
#         new_weight_matrices
#
#     def test_weight_matrices_setter_raises_on_incorrect_input(self):


class TestResultsCorrectness(object):
    def test_regular_nn_response_correctness(self):
        example_output = self.nn_input_vector
        for weight_matrix in self.weight_matrices:
            example_output = scipy.special.expit(example_output.dot(weight_matrix))

        nn_output = self.nn.feed(self.nn_input_vector)

        self.assertTrue(np.allclose(nn_output, example_output))

    def test_stacked_nn_response_correctness(self):
        # Inputs to stacked NN are independent row-wise
        # so the response should look the same when calulated one row at a time or
        output_vectors = []
        for i in range(len(self.nn_input_matrix)):
            output_v = self.nn_input_matrix[i]
            for weight_matrix in self.weight_matrices:
                output_v = scipy.special.expit(output_v.dot(weight_matrix))
            output_vectors.append(output_v)

        example_ouput = np.vstack(output_vectors)

        nn_output = self.nn.feed(self.nn_input_matrix)

        self.assertTrue(np.allclose(example_ouput, nn_output))


class TestNumpyNNResultCorrectness(unittest.TestCase, TestResultsCorrectness, GenericNumpyNNTest):
    def setUp(self):
        GenericNumpyNNTest.setUp(self)


class TestNNDeepCopy(object):
    def test_equality_operator_deepcopy_is_equal_to_original(self):
        nn_deepcopy = deepcopy(self.nn)
        self.assertEqual(self.nn, nn_deepcopy)
        self.assertIsNot(self.nn, nn_deepcopy)

    def test_sanity_compare_to_other_nn(self):
        nn_deepcopy = deepcopy(self.nn)
        self.assertNotEqual(nn_deepcopy, self.other_nn)
        self.assertIsNot(nn_deepcopy, self.other_nn)


class TestNumpyNNDeepCopy(unittest.TestCase, TestNNDeepCopy, GenericNumpyNNTest):
    def setUp(self):
        GenericNumpyNNTest.setUp(self)


class TestNNPickling(object):
    def setUp(self):
        self.pickle_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "nn.pickle")
        with open(self.pickle_file, mode="wb") as pickle_file:
            pickle.dump(self.nn, pickle_file)

    def test_equality_of_unpickled_nn(self):
        with open(self.pickle_file, mode="rb") as pickle_file:
            unpickled_nn = pickle.load(pickle_file)

        self.assertEqual(unpickled_nn, self.nn)

    def test_sanity_compare_against_different_nn(self):
        with open(self.pickle_file, mode="rb") as pickle_file:
            unpickled_nn = pickle.load(pickle_file)

        self.assertNotEqual(unpickled_nn, self.other_nn)

    def tearDown(self):
        os.remove(self.pickle_file)


class TestNumpyNNPickling(unittest.TestCase, TestNNPickling, GenericNumpyNNTest):
    def setUp(self):
        GenericNumpyNNTest.setUp(self)
        TestNNPickling.setUp(self)

    def tearDown(self):
        TestNNPickling.tearDown(self)


class TestNNEqualityOperator(object):
    def setUp(self):
        self.available_nn_classes = [NumpyNN]

    def test_nn_instance_equals_self(self):
        self.assertEqual(self.nn, self.nn)

    def test_nn_instance_equals_own_deepcopy(self):
        self.assertEqual(self.nn, deepcopy(self.nn))

    def test_nn_with_identical_weight_matrices_is_equal(self):
        other_nn_from_weight_matrices = self.tested_network_class.from_weight_matrices(self.nn.weight_matrices)
        self.assertEqual(self.nn, other_nn_from_weight_matrices)

    def test_partially_similar_nn_is_not_equal(self):
        other_nn_from_size_data = self.tested_network_class.from_size_data(self.nn_inputs, self.nn_layer_sizes)
        self.assertNotEqual(self.nn, other_nn_from_size_data)

    def test_nn_with_changed_weights_does_not_equal_the_original(self):
        other_nn = deepcopy(self.nn)
        other_nn._layer_weights = NeuralNetwork.generate_random_weights(self.nn_inputs, self.nn_layer_sizes)
        self.assertNotEqual(self.nn, other_nn)

    def test_nn_of_another_class_with_identical_parameters_is_not_equal(self):
        for nn_class in filter(lambda x: x is not self.tested_network_class, self.available_nn_classes):
            other_nn_from_size_info = nn_class.from_size_data(self.nn_inputs, self.nn_layer_sizes)
            other_nn_from_weight_matrices = nn_class.from_weight_matrices(self.weight_matrices)
            self.assertNotEqual(self.nn, other_nn_from_weight_matrices)
            self.assertNotEqual(self.nn, other_nn_from_size_info)


class TestNumpyNNEqualityOperator(unittest.TestCase, GenericNumpyNNTest, TestNNEqualityOperator):
    def setUp(self):
        GenericNumpyNNTest.setUp(self)
        TestNNEqualityOperator.setUp(self)


if __name__ == '__main__':
    unittest.main()
