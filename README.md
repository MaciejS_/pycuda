# README #

This repository contains an implementation of a simple feed-forward neural network 
and a custom implentation of the backpropagation algorithm.

### How do I get set up? ###

This project was developed under **Python 3.4.5**

You'll need the following packages:
* PyCUDA == 2016.1.2+cuda7518 (for PyCUDA installation instructions please refer to the official documentation)
* numpy >= 1.11.1
* scipy >= 0.18.0
* parameterized >= 0.61 (for tests)

To check whether the setup on your local machine was successful, run tests. All should pass.