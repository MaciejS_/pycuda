import numpy as np
import scipy.special


class NeuralNetwork:

    def __init__(self, inputs: int, layer_sizes: tuple, weight_matrices: tuple, activation_functions: tuple):
        """
        Creates a numpy-based neural network.

        Weight matrices determine the structure of neural network.
        A single weight matrix determines the structure of a neuron layer
        Number of columns in a weight matrix corresponds to the number of neurons in that layer
        Number of rows corresponds to the number of inputs to this layer

        The first weights matrix represents the weights of input connections.

        :param inputs: int, number of inputs to the network
        :param layer_sizes: tuple, a set of layer sizes
        :param weight_matrices: tuple of 2D np.ndarray matrices
        :param activation_functions: tuple of callables
        """
        self._validate_instance_parameters(inputs, layer_sizes, weight_matrices, activation_functions)

        self._inputs = inputs
        self._layer_sizes = layer_sizes

        self._activation_functions = activation_functions
        self._layer_weights = weight_matrices

        pass

    @classmethod
    def from_weight_matrices(cls, weight_matrices: tuple, activation_functions: tuple):
        """
        Creates a neural network based on connection weights matrices.

        The structure of a neuron layer is inferred from the shape of a corresponding weight matrix.
        Number of columns in a weight matrix corresponds to the number of neurons in that layer
        Number of rows determines the number of inputs to this layer

        If `activation_functions` is None, sigmoid functions will be used.

        :param weight_matrices: tuple of 2D np.ndarray
        :param activation_functions: optional tuple, determines activation functions for individual neuron layers
        """

        inputs = weight_matrices[0].shape[0]
        layer_sizes = [weight_matrix.shape[1] for weight_matrix in weight_matrices]

        return cls(inputs, layer_sizes, weight_matrices, activation_functions)

    @classmethod
    def from_size_data(cls, inputs: int, layer_sizes: tuple, activation_functions: tuple, weights_range: tuple=(-1, 1)):
        """
        Generates a neural network from information about its size.
        Connection weight values are generated randomly using uniform distribution

        :param inputs: int, number of inputs to the first layer
        :param layer_sizes: tuple of int, sizes of consecutive neuron layers
        :param activation_functions: list of any, list of activation functions for consecutive layers
        :return: NeuralNetwork object
        """
        if not all(x > 0 for x in (layer_sizes + (inputs,))):
            raise ValueError("All layers must have size of at least 1")

        return cls(inputs,
                   layer_sizes,
                   cls.generate_random_weights(inputs, layer_sizes, weight_range=weights_range),
                   activation_functions)

    # @classmethod
    # def from_other_nn(cls,
    #                   neural_network,
    #                   with_weights: bool = False):
    #     """
    #     Create another instance of neural network based on `neural_network` argument
    #     If `with_weights` is False a new set of weights will be generated for this NN,
    #     otherwise, the returned network will be a clone of `neural_network`
    #     Activation functions are copied from `neural_network`
    #
    #
    #     :param neural_network: NeuralNetwork object, returned NN will have the same structure as this network
    #     :param with_weights: optional bool, if True, `neural_network` is cloned, otherwise a new set of weights will be generated
    #     :return: NeuralNetwork
    #     """
    #
    #     if not with_weights:
    #         return cls.from_size_data(neural_network.inputs,
    #                                   neural_network.layer_sizes,
    #                                   neural_network.ac)

    def feed(self, inputs: np.ndarray) -> np.ndarray:
        """
        Feeds the `inputs` to the neural network and returns the responses of output layer neurons
        This function needs to be overridden in subclasses
        :param inputs: np.ndarray, may either be a 1D vector or a matrix which last dimension mathes the number of inputs
        :return: np.ndarray, shape
        """
        raise NotImplementedError

    @staticmethod
    def generate_random_weights(inputs: int,
                                layer_sizes: tuple,
                                datatype=np.float32,
                                weight_range: tuple=(-1.0, 1.0),
                                ) -> list:
        """
        Generates a set of random weight matrices for this NeuralNetwork
        Weights are generated using uniform distribution

        :param inputs: int, number of inputs to the first layer
        :param layer_sizes: tuple of int, sizes of consecutive neuron layers
        :param weight_range: tuple, inclusive range of weights
        :param datatype: type, determines the numeric datatype that will hold the generated values e.g. theano.config.floatX or numpy.float64
        :return: list of 2D np.ndarray
        """

        ext_layer_sizes = (inputs,) + tuple(layer_sizes)

        return [NeuralNetwork._generate_weights_matrix(neurons, inputs, datatype, *weight_range)
                for inputs, neurons
                in zip(ext_layer_sizes, ext_layer_sizes[1:])]

    @staticmethod
    def _generate_weights_matrix(neurons: int,
                                 inputs: int,
                                 datatype=np.float32,
                                 min_value: float=-1,
                                 max_value: float=1,
                                 ) -> np.ndarray:
        """
        Creates a weights matrix and fills it with random values in range (min_value, max_value)
        Values are generated using uniform distribution

        Shape of returned matrix, (Y, X) corresponds to (`inputs`, `neurons`) values

        :param neurons: X axis size of the generated matrix, equal to the number of inputs to this neural layer
        :param inputs: Y axis size of generated matrix, equal to the number of neurons in a neural layer
        :param min_value: float, minimum possible value of a generated weight
        :param max_value: float, maximum possible value of a generated weight
        :param datatype: type, determines the numeric datatype that will hold the generated values e.g. theano.config.floatX or numpy.float64
        :return: 2D numpy.ndarray, a matrix of shape (`inputs`, `neurons`)
        """
        value_range = max_value - min_value
        return np.asarray(np.random.rand(inputs, neurons) * value_range + min_value, dtype=datatype)

    @staticmethod
    def _check_weight_matrices_compatibility(weight_matrices: tuple) -> bool:
        """
        Returns True if shapes of neighboring weight matrices are compatible

        :param weight_matrices: tuple of 2D np.ndarray.
        :return: bool
        """
        for prev, current in zip(weight_matrices, weight_matrices[1:]):
            if current.shape[0] != prev.shape[1]:
                return False
        return True

    @staticmethod
    def ravel_weight_matrices(weight_matrices: list) -> np.ndarray:
        """
        Converts a list of weight matrices extracted from neural layers to a single vector
        Conversion is done via ravelling, the resulting vectors are concatenated in the order of appearance on list

        :param weight_matrices: list of np.ndarray
        :return: 1D np.ndarray
        """
        weight_vectors = [np.ravel(matrix) for matrix in weight_matrices]
        return np.concatenate(weight_vectors)

    def unravel_weights_vector(self, weights_vector: np.ndarray) -> list:
        """
        Converts a weights vector back to a set of weight matrices
        :param weights_vector: 1D np.ndarray
        :return: list of 2D np.ndarrays
        """
        matrix_shapes = (matrix.shape for matrix in self.weight_matrices)

        matrices, caret = [], 0
        for shape in matrix_shapes:
            slice_length = np.prod(shape)
            matrices.append(weights_vector[caret:caret+slice_length].reshape(shape))
            caret += slice_length

        return matrices

    @property
    def inputs(self) -> int:
        """
        Returns the number of inputs of this network

        :return:
        """
        return self._inputs

    @property
    def layer_sizes(self) -> tuple:
        """
        Returns layer sizes as a list

        :return:
        """
        return self._layer_sizes

    @property
    def activation_functions(self) -> tuple:
        """
        Returns a sequence of activation functions used by consecutive layers of this neural network

        :return: tuple of callable
        """
        return self._activation_functions

    @property
    def weight_matrices(self) -> list:
        """
        Returns the weight matrices used by neuron layers of this network
        Needs to be overridden by subclass

        :return: tuple of np.ndarray
        """
        raise NotImplementedError

    @weight_matrices.setter
    def weight_matrices(self, weight_matrices: tuple) -> None:
        """
        Updates weight matrices of internal neuron layers with matrices in `weight_matrices`
        ValueError will be raised if number of passed matrices is not equal to the number of neuron layers in this network

        Needs to be overridden by subclass

        :param weight_matrices: tuple of np.ndarray
        :return: None
        """
        raise NotImplementedError


    def _validate_instance_parameters(self, inputs, layer_sizes, weight_matrices, activation_functions) -> None:
        """
        This method runs a series of checks against NN parameters and raises ValueError if they happen to be invalid
        :param inputs: int, the number of inputs to this NN
        :param layer_sizes: list/tuple of int, list of neuron layer sizes
        :param weight_matrices: list of np.ndarray, weight matrices for consecutive connection layers
        :param activation_functions: list of any, list of activation functions for corresponding layers
        :return:
        """
        if inputs < 1 or layer_sizes is None or not all(size > 0 for size in layer_sizes):
            raise ValueError("Size of each layer must be >0, the same goes for inputs")
        if inputs != weight_matrices[0].shape[0]:
            raise ValueError("First weight matrix dimensions don't match the declared size of inputs")
        if len(layer_sizes) != len(weight_matrices):
            raise ValueError("Number of weight matrices must be equal to number of layers")
        if not self._check_weight_matrices_compatibility(weight_matrices):
            raise ValueError("Weight matrices shapes are not intercompatible")
        if len(activation_functions) != len(weight_matrices):
            raise ValueError("Missing activation functions: %d layers but %d functions"
                             % (len(weight_matrices), len(activation_functions)))

    def __eq__(self, other):
        if type(self) is not type(other):
            return False
        try:
            equal = self._inputs == other._inputs
            equal &= np.all(self._layer_sizes == other._layer_sizes)
            if not len(self._layer_weights) == len(other._layer_weights):
                return False
            for mat1, mat2 in zip(self._layer_weights, other._layer_weights):
                if not np.allclose(mat1, mat2):
                    return False
            equal &= self._activation_functions == other._activation_functions
            return equal
        except Exception as exc:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

class NumpyNN(NeuralNetwork):

    def feed(self, inputs: np.ndarray) -> np.ndarray:
        """
        Feeds the `inputs` to the neural network and returns the responses of output layer neurons
        :param inputs: np.ndarray, may either be a 1D vector or a matrix which last dimension mathes the number of inputs
        :return: np.ndarray, a 1D vector
        """
        input_vector = np.copy(inputs)
        for layer_weights, activation_function in zip(self._layer_weights, self._activation_functions):
            input_vector = activation_function(input_vector.dot(layer_weights))
        return input_vector

    @classmethod
    def from_weight_matrices(cls, weight_matrices: tuple, activation_functions: tuple=None):
        """
        Creates a neural network based on connection weights matrices.

        The structure of a neuron layer is inferred from the shape of a corresponding weight matrix.
        Number of columns in a weight matrix corresponds to the number of neurons in that layer
        Number of rows corresponds to the number of inputs to this layer

        If `activation_functions` is None, sigmoid functions will be used.

        :param weight_matrices: tuple of 2D np.ndarray
        :param activation_functions: optional tuple, determines activation functions for individual neuron layers
        """
        if not activation_functions:
            activation_functions = (scipy.special.expit,) * len(weight_matrices)

        return super().from_weight_matrices(weight_matrices, activation_functions)

    @classmethod
    def from_size_data(cls, inputs: int, layer_sizes: tuple, activation_functions: tuple=None):
        """
        Generates a neural network from information about its size.
        Connection weight values are generated randomly using uniform distribution

        :param inputs: int, number of inputs to the first layer
        :param layer_sizes: tuple of int, sizes of consecutive neuron layers
        :param activation_functions: list of any, list of activation functions for consecutive layers
        :return: NeuralNetwork object
        """
        if not activation_functions:
            activation_functions = (scipy.special.expit,) * len(layer_sizes)

        return super().from_size_data(inputs, layer_sizes, activation_functions)

    @property
    def weight_matrices(self) -> list:
        """
        Returns the weight matrices used by neuron layers of this network
        :return: tuple of np.ndarray
        """
        return tuple(self._layer_weights)

    @weight_matrices.setter
    def weight_matrices(self, weight_matrices: tuple) -> None:
        """
        Updates weight matrices of internal neuron layers with matrices in `weight_matrices`
        ValueError will be raised if number of passed matrices is not equal to the number of neuron layers in this network

        :param weight_matrices: tuple of np.ndarray
        :return: None
        """
        if weight_matrices is not None and len(weight_matrices) == len(self._layer_weights):
            if all(new_matrix.shape == old_matrix.shape
                   for new_matrix, old_matrix
                   in zip(self._layer_weights, weight_matrices)):
                self._layer_weights = weight_matrices
            else:
                raise ValueError("New weight matrices are misshaped")
        else:
            raise ValueError("Failed to update weight matrices: %d layers but %d matrices" %
                             (len(self._layer_weights), len(weight_matrices)))

    @staticmethod
    def generate_random_weights(inputs: int,
                                layer_sizes: tuple,
                                weight_range: tuple = (-1.0, 1.0),
                                datatype=np.float32) -> list:
        return NeuralNetwork.generate_random_weights(inputs=inputs,
                                               layer_sizes=layer_sizes,
                                               weight_range=weight_range,
                                               datatype=datatype)

    @staticmethod
    def _generate_weights_matrix(neurons: int,
                                 inputs: int,
                                 datatype=np.float32,
                                 min_value: float = -1,
                                 max_value: float = 1) -> np.ndarray:
        return NeuralNetwork._generate_weights_matrix(neurons=neurons,
                                                inputs=inputs,
                                                datatype=datatype,
                                                min_value=min_value,
                                                max_value=max_value)


