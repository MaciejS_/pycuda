from nn.neural_network.activation_functions.relu import ReLU
from nn.neural_network.network.network import NeuralNetwork, NumpyNN

import numpy as np

class LearningNN(NeuralNetwork):
    """
    Base class for learning NNs, defines the interface for concrete implementations.
    """

    def __init__(self):
        raise NotImplementedError


    def feed(self, inputs: np.ndarray):
        # the implementation here needs to create and keep all the intermediate values so that they can be reused during backprop
        raise NotImplementedError

    def backpropagate(self, errors):
        """

        :param errors:
        :return:
        """
        raise NotImplementedError

    def learnOn(self, training_examples, expected_outputs):
        for example, expected in zip(training_examples, expected_outputs):
            actual_output = self.feed(example)
            # actual_output = actual_output.squeeze()
            self.backpropagate(expected - actual_output)

class LearningNumpyNN(LearningNN, NumpyNN):
    """
    This NN class keeps track of all the activation values to all of neurons, so that backpropagation can be carried out
    """

    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple,
                 learning_rate: float=0.01):
        # LearningNN super is not called because LearningNN doesn't do anything in its __init__
        super(NumpyNN, self).__init__(inputs, layer_sizes, weight_matrices, (None, ) * len(weight_matrices))
        # TODO fix bug, `learning_rate` param accepts the value of `activation_functions` leading to all sorts of errors
        self._learning_rate = learning_rate

        # raw inputs are the values of inputs before weights are applied to them
        # these include the inputs to the network as well as outputs of each layer of neurons
        self._raw_inputs = None

        # weighted inputs are the values
        self._weighted_inputs = None

    @classmethod
    def from_weight_matrices(cls,
                             weight_matrices: tuple,
                             activation_functions: tuple = None,
                             learning_rate: float = 0.01):
        if activation_functions is not None:
            raise NotImplementedError("Using custom activation function is not supported yet")

        instance = super().from_weight_matrices(weight_matrices, activation_functions)
        instance._learning_rate = learning_rate
        instance._raw_inputs = None
        instance._weighted_inputs = None

        return instance

    @classmethod
    def from_size_data(cls,
                       inputs: int,
                       layer_sizes: tuple,
                       activation_functions: tuple = None,
                       learning_rate: float = 0.01):
        if activation_functions is not None:
            raise NotImplementedError("Using custom activation function is not supported yet")

        instance = super().from_size_data(inputs, layer_sizes, activation_functions)
        instance._learning_rate = learning_rate
        instance._raw_inputs = None
        instance._weighted_inputs = None
        return instance

    @property
    def learning_rate(self):
        return self._learning_rate

    @learning_rate.setter
    def learning_rate(self, new_learning_rate):
        self._learning_rate = new_learning_rate

    def _create_intermediate_matrices(self):
        """
        Intermediate matrices hold the values of all inputs to neurons in
        :return:
        """
        # TODO: prepare the lists of matrices and the matrices within, to reduce time spent on allocations
        raise NotImplementedError

    def feed(self, inputs: np.ndarray) -> np.ndarray:
        """
        Feeds the `inputs` to the neural network and returns the responses of output layer neurons
        :param inputs: np.ndarray, may either be a 1D vector or a matrix which last dimension matches the number of inputs
        :return: np.ndarray, a 1D vector
        """
        if len(inputs.shape) > 1:
            raise ValueError("inputs must be a 1D vector, perhaps you should use np.squeeze to reduce dimensionality")

        input_vector = inputs
        self._weighted_inputs = []
        self._raw_inputs = [inputs]
        for layer_weights in self._layer_weights:
            broadcasted_input_vector = np.broadcast_to(np.atleast_2d(input_vector).T, layer_weights.shape)
            current_layer_inputs = np.multiply(broadcasted_input_vector, layer_weights)

            inputs_weights_dot_product = np.sum(current_layer_inputs, axis=0)
            self._weighted_inputs.append(inputs_weights_dot_product)  # retain current_layer_inputs for backpropagation

            input_vector = ReLU.apply(inputs_weights_dot_product)

            self._raw_inputs.append(input_vector)       # retain outputs for backprop
        return input_vector

    def learnOn(self, training_examples, expected_outputs):
        for example, expected in zip(training_examples, expected_outputs):
            actual_output = self.feed(example)
            self.backpropagate(expected - actual_output)

    def agh_backprop(self, errors: np.ndarray) -> None:
        """
        Backpropagation algorithm as described in http://home.agh.edu.pl/~vlsi/AI/backp_t_en/backprop.html
        :param errors: the difference between ground truth and NN prediction
        :return: None
        """

        error_values = errors.copy()
        reversed_layer_errors = [error_values]
       # first weight matrix is skipped because errors don't propagate to the input layer
        for weights_matrix in reversed(self.weight_matrices[1:]):
            error_values = error_values.dot(weights_matrix.T)
            reversed_layer_errors.append(error_values)

        weights_matrices_updates = []
        layer_errors = reversed(reversed_layer_errors)
        for x, h, e in zip(self._raw_inputs, self._weighted_inputs, layer_errors):
            # TODO: assure that the vectors passed here are actually 2D to avoid broadcasting all the time
            horizontal_vector = np.atleast_2d(self._learning_rate * e * ReLU.apply_derivative(h))
            weights_matrix_update = np.atleast_2d(x).T.dot(horizontal_vector)
            weights_matrices_updates.append(weights_matrix_update)
            # a bit of explanation:
            # the errors vector `e` should be horizontal
            # raw inputs `x` and df(weighted_inputs) should be elementwise multiplied together
            # above two should be gemmed e.dot(x*df(h)) and this will give an update matrix compatible with the weights matrix

        # update matrices
        self.weight_matrices = [matrix + update
                                for matrix, update
                                in zip(self.weight_matrices, weights_matrices_updates)]

    def backpropagate(self, errors) -> None:
        self.agh_backprop(errors)