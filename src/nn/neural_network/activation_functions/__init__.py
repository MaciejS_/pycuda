import numpy as np

class ActivationFunction:

    @staticmethod
    def apply(values: np.ndarray) -> np.ndarray:
        """
        Applies the activation function to all values and returns a copy of the input matrix

        This method needs to be overridden by subclasses

        :param values:
        :return: result of applying this activation function to `value`
        """
        raise NotImplementedError

    @staticmethod
    def apply_in_place(values: np.ndarray) -> np.ndarray:
        """
        Applies the activation function to all values and returns a copy of the input matrix

        This method needs to be overridden by subclasses

        :param values:
        :return: result of applying this activation function to `value`
        """
        raise NotImplementedError

    @staticmethod
    def apply_derivative(values: np.ndarray) -> np.ndarray:
        """
        Applies the derivative of the activation function to all values
        Used primarily by the backpropagation algorithm

        This method needs to be overridden by subclasses

        :param  values:
        :return: result of applying this activation function dereivative to `value`
        """
        raise NotImplementedError

    @staticmethod
    def apply_derivative_in_place(values: np.ndarray) -> np.ndarray:
        """
        Applies the derivative of the activation function to all values, in place
        Used primarily by the backpropagation algorithm

        This method needs to be overridden by subclasses

        :param  values:
        :return: result of applying this activation function dereivative to `value`
        """
        raise NotImplementedError