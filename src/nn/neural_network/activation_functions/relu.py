from nn.neural_network.activation_functions import ActivationFunction
import numpy as np

class ReLU(ActivationFunction):
    """
    ReLU is defined as `x if x > 0 else 0`
    Derivative of ReLU is defined as `1 if x > 0 else 0`
    """

    @staticmethod
    def apply(values: np.ndarray) -> np.ndarray:
        values_copy = np.copy(values)
        return ReLU.apply_in_place(values_copy)
    
    @staticmethod
    def apply_in_place(values: np.ndarray) -> np.ndarray:
        values[values < 0] = 0
        return values

    @staticmethod
    def apply_derivative(values: np.ndarray):
        values_copy = np.copy(values)
        return ReLU.apply_derivative_in_place(values_copy)

    @staticmethod
    def apply_derivative_in_place(values: np.ndarray) -> np.ndarray:
        values[values > 0] = 1
        values[values < 0] = 0
        return values

