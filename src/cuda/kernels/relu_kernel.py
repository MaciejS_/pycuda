import pycuda
from pycuda.gpuarray import GPUArray

import numpy as np

from cuda.kernels.kernel_module import KernelModule
from cuda.kernels import KERNELS_ROOT


class ReLUKernel(KernelModule):
    """
    Convenience wrapper over a raw relu kernel
    """

    def __init__(self):
        super().__init__(r"%s\relu.cu" % KERNELS_ROOT)
        self._relu_kernel = self._pycuda_source_module.get_function('relu')
        self._relu_derivative = self._pycuda_source_module.get_function('relu_derivative')

    def apply(self,
              input_array: GPUArray,
              output_array: GPUArray = None,
              cuda_block: tuple = None) -> GPUArray:
        """
        Applies ReLU to every value in `input_array`.
        :param input_array:
        :param output_array: result will be stored here, if None, `output_array` will be created automatically
        :param cuda_block: dimensions of a CUDA block used for this operation
        :return: `output_array`
        """
        output_array = output_array or pycuda.gpuarray.empty_like(input_array)
        cuda_block = cuda_block or KernelModule.block_dim_for_ndarray(input_array)

        if len(input_array.shape) > 2:
            raise NotImplementedError("ReLU kernel for >2D matrices is not ready yet")

        width = np.int32(input_array.shape[-1])
        size = np.int32(input_array.size)

        self._relu_kernel(input_array, output_array,
                          width, size,
                          block=cuda_block)
        return output_array

    def apply_derivative(self,
                         input_array: GPUArray,
                         output_array: GPUArray = None,
                         cuda_block: tuple = None) -> GPUArray:
        """
        Applies ReLU derivative to every value in `input_array`.

        ReLU derivative is defined as `1 if x > 0 else 0`

        :param input_array:
        :param output_array: result will be stored here, if None, `output_array` will be created automatically
        :param cuda_block: dimensions of a CUDA block used for this operation
        :return: `output_array`
        """
        output_array = output_array or pycuda.gpuarray.empty_like(input_array)
        cuda_block = cuda_block or KernelModule.block_dim_for_ndarray(input_array)

        if len(input_array.shape) > 2:
            raise NotImplementedError("ReLU derivative kernel for >2D matrices is not ready yet")

        width = np.int32(input_array.shape[-1])
        size = np.int32(input_array.size)

        self._relu_derivative(input_array, output_array,
                              width, size,
                              block=cuda_block)
        return output_array
