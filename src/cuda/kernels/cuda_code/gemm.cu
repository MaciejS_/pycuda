/*
 *  This function computes a single cell of A.dot(B) and returns its value
 *  Effectively, this function performs vector multiplication
 *  cellX, cellY determine the coordinates of a cell to be calculated
*/
__device__ float device_gemm_single_cell(float* A, float* B,         \
                                          int widthA, int widthB,     \
                                          int cellX, int cellY) {

    // inner dimension of the matrices is equal to widthA and heightB
    // where A is the left and B is the right operand of GEMM
    float c = 0;
    for (int i = 0; i < widthA; i++) {
        c += A[cellY * widthA + i] * B[i * widthB + cellX];
    }
    return c;
}

/*
 *  This function computes a single cell of A.dot(B.T) and returns its value
 *  Matrix B is temporarily transposed inside this function (it is iterated over horizontally, instead of vertically)
 *  cellX, cellY determine the coordinates of a output matrix cell to be calculated
 *  This function does not perform any dimension correctness checks
*/
__device__ float device_gemm_single_cell_with_transposedB(float* A, float* B,       \
                                                         int widthA, int widthB,    \
                                                         int cellX, int cellY) {
    float c = 0;
    for (int i = 0; i < widthA; i++) {
        // because of the simulated transposition here, B array is iterated row-wise (whereas in regular gemm it should be iterated over colwumnwise)
        c += A[cellY * widthA + i] * B[cellX * widthB + i];
        // in other words, both arrays are iterated over horizontally
    }
    return c;
}

/*
 *  This function computes a single cell of (A.T).dot(B) and returns its value
 *  Matrix A is temporarily transposed inside this function (it is iterated over vertically, instead of horizontally)
 *  cellX, cellY determine the coordinates of a output matrix cell to be calculated
 *  This function does not perform any dimension correctness checks
*/
__device__ float device_gemm_single_cell_with_transposedA(float* A, float* B,       \
                                                         int widthA, int widthB,    \
                                                         int heightA,               \
                                                         int cellX, int cellY) {
    // inner dimension of the matrices is equal to wA (width of A) and hB (height of B)
    // where A is the left and be is the right operand of GEMM
    float c = 0;
    for (int i = 0; i < heightA; i++) {
        // in other words, both arrays are iterated over vertically
        c += A[i * widthA + cellY] * B[i * widthB + cellX];
    }
    return c;
}

/*
    Matrix multiplication kernel
    C = A*B
    wA, wB - width of matrices A and B
    hA, hB - height of matrices A and B

    This kernel assumes that PyCUDA GPUArrays are unrolled row-wise
*/
__global__ void simple_gemm(float* A, float* B, float* C,   \
                            int wA, int hA,                 \
                            int wB, int hB) {

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    // calculate the value of a single cell
    C[threadY * wB + threadX] = device_gemm_single_cell(A, B, wA, wB, threadX, threadY);
}

__global__ void shared_memory_gemm(float* A, float* B, float* C,    \
                                    int wA, int hA,                 \
                                    int wB, int hB) {


}

__global__ void gemm_with_transposedB(float* A, float* B, float* C,   \
                                    int wA, int hA,              \
                                    int wB, int hB) {
    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    C[threadY * hB + threadX] = device_gemm_single_cell_with_transposedB(A, B,
                                                                    wA, wB,
                                                                    threadX, threadY);

}