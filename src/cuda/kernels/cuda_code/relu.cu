
__forceinline__ __device__ float device_relu(float value) {
    return value > 0 ? value : 0;
}

__forceinline__ __device__ float device_relu_derivative(float value) {
    return value > 0 ? 1 : 0;
}


/*
    1D ReLU kernel
    Applies the ReLU function to all values in `in` vector
*/
__global__ void vector_relu(float* in, float* out, int n_values){

    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < n_values) {
        out[i] = in[i] > 0 ? in[i] : 0;
    }
}

/*
    2D ReLU kernel
    Applies the ReLU function to all values in `in` vector
*/
__global__ void relu(float* in, float* out, int width, int n_values){

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    int i = threadY * width + threadX;

    if (i < n_values) {
        out[i] = device_relu(in[i]);
    }
}

/*
    2D ReLU derivative kernel
    Applies the derivative of ReLU function to all values in `in` vector
*/
__global__ void relu_derivative(float* in, float* out, int width, int n_values){

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    int i = threadY * width + threadX;

    if (i < n_values) {
        out[i] = device_relu_derivative(in[i]);
    }
}