#include <relu.cu>
#include <gemm.cu>

/*

    weighted_input_matrices will be filled with activation values of neurons
    raw_input_matrices contain the inputs vector and outputs of each neuron layer
*/
__global__ void feed_matrix(float** weight_matrices,        \
                             float** raw_input_matrices,    \
                             float** weighted_input_matrices, \
                             int n_weight_matrices,         \
                             int* weight_matrix_dims,       \
                             int input_width, int input_height) {

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    int imX = input_width;
    int imY = input_height;

    int omX, omY;

    for (int i = 0; i < n_weight_matrices; i++) {
        float* weight_matrix = weight_matrices[i];

        // weight matrix dimensions
        int wmY = weight_matrix_dims[2*i];
        int wmX = weight_matrix_dims[(2*i) + 1];

        // intermediate vectors include inputs and all vectors that result from GEMV
        // len(intermediate_vectors) = n_weight_matrices + 1
        float* inputs_matrix = raw_input_matrices[i];
        float* output_matrix = raw_input_matrices[i+1];
        float* weighted_input_matrix = weighted_input_matrices[i];

        // output matrix dimensions
        omX = wmX;
        omY = imY;

        int cell_idx = threadY * omX + threadX;
        // CUDA block must be adjusted to the largest X and Y dim
        if ((threadX < omX) && (threadY < omY)) {
            float cell = device_gemm_single_cell(inputs_matrix, weight_matrix, \
                                                              imX, wmX,  \
                                                              threadX, threadY);
            weighted_input_matrix[cell_idx] = cell;
            output_matrix[cell_idx] = device_relu(cell);
        }

        // update input matrix dimensions
        imY = omY;
        imX = omX;

        // synchronize threads so that layers are computed one by one
        __syncthreads();
    }
}

/*
 * Backpropagation kernel
 * Updates connection weights based on output errors
 * weights matrices will be modified as a result of this function
*/
__global__ void backpropagate_vector(float** weighted_input_matrices, \
                                    float** raw_input_matrices,      \
                                    float** intermediate_error_matrices,\
                                    float** weight_matrices,         \
                                    int n_weight_matrices,           \
                                    int* weight_matrix_dims,         \
                                    float learning_rate              \
                                    ) {
        //TODO: intermediate errors matrices must be assembled like intermediate matrices but with errors as the last elemennt (and skipping the first feedwise)

        // data:
        // raw inputs: inputs vector and output vectors of neuron layers (paradoxically)
        // weighted inputs: inputs after they have passed through weighted connections but before application of activation function
        // temporary matrices to hold error values (same size as `intermediate vectors`)

    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;    // threadY is needed here, because we ARE operation on matrices during update phase



// 1. backpropagate errors
// this is basically feed_matrix but in reverse, with transposed matrices I could reuse feed_matrix here
// maybe not, because that's a __global__ function, not a __device__ one

// iterating over a transposed matrix means that we're iterating vertically where we used to iterate horizontally, and vice-versa

    int imX = weight_matrix_dims[2*n_weight_matrices - 1]; // that's errors vector after all, it will have the dimensions of the last matrix
    int imY = 1;    // 1 because we're propagating a vector

    int omX, omY;

    // iterating in reverse
    // i > 0 because errors don't propagate past the input weights matrix (we have nothing to apply that vector to afterwards)
    for (int i = n_weight_matrices-1; i > 0; i--) {
        float* weight_matrix = weight_matrices[i];

        // weight matrix dimensions (checked, will work in reverse)
        int wmY = weight_matrix_dims[2*i];
        int wmX = weight_matrix_dims[(2*i) + 1];

        // intermediate vectors include inputs and all vectors that result from GEMV
        // len(intermediate_vectors) = n_weight_matrices + 1
        // (checked, will work in reverse)
        float* inputs_matrix = intermediate_error_matrices[i];
        float* output_matrix = intermediate_error_matrices[i-1];

        // output matrix dimensions
        omX = wmY; // we are using TRANSPOSED weight matrices, so the outputX will be equal to Y of the weigh matrix
        omY = imY; // that seems correct, because this param should always be 1 (backpropping a vector)

        // cell idx remains unchanged because we're still writing to a horizontal vector
        int cell_idx = threadY * omX + threadX; // that seems correct

        // CUDA block must be adjusted to the largest X and Y dim
        // TODO: make sure there's no mismatch here - transposed matrices
        if ((threadX < omX) && (threadY < omY)) {
            output_matrix[cell_idx] = device_gemm_single_cell_with_transposedB(inputs_matrix, weight_matrix,\
                                                                                  imX, wmX,                 \
                                                                                  threadX, threadY);
        }
        // synchronize threads so that layers are computed one by one
        __syncthreads();
        // update input matrix dimensions
        imY = omY;
        imX = omX;
    }

// 2. generate updates to weights
// * I will need another set of matrices,
// * or I can update the weight in place
// I need to iterate over error matrices in reverse

    for (int i = 0; i < n_weight_matrices; i++) {
        float* weight_matrix = weight_matrices[i];

        int wmY = weight_matrix_dims[2*i];
        int wmX = weight_matrix_dims[(2*i) + 1];

        // iterate over errors_matrices list as usual, errors_vector[0] should be applied to weights_matrix[0]
        // raw_inputs[-1] is skipped because that's actually the OUTPUTS vector and is of no use (nothing to update with)
        float* raw_inputs = raw_input_matrices[i];  // dimensionality matches i-th weights matrix
        float* weighted_inputs = weighted_input_matrices[i];    // dimensionality matches i+1 weights matrix
        float* layer_errors = intermediate_error_matrices[i];   // dimensionality matches i+1 weights matrix

        int leX = wmX;  // layer_errors_X, this dimension matches the output dim of current weight matrix (X dim)
        int leY = 1;    // layer_errors_y = 1 because the input is a vector, and therefore all raw inputs are vectors

        int riX = wmY;  // raw_input_X, this dimension matches the input dim of current weights matrix (Y dim)
        int riY = 1;    // because the input is a vector, and all intermediate ones will be vectors too

        int cell_idx = threadY * wmX + threadX; // that seems correct

        // prepare the horizontal vector of the calculation
        if(threadX < leX && threadY == 0) {
            layer_errors[threadX] *= device_relu_derivative(weighted_inputs[threadX]) * learning_rate;
        }
        __syncthreads();

        // we're recreating the weights adjustment matrix from two vectors here,
        // so indices used to operate on both need to be distinctive (and I need to get them correctly)
        if ((threadX < wmX) && (threadY < wmY)) {
            // calculate the update values and immediately apply them to weights matrix
            weight_matrix[cell_idx] += device_gemm_single_cell_with_transposedA(raw_inputs, layer_errors, \
                                                                                  riX, leX, riY,                \
                                                                                  threadX, threadY);
        }
        __syncthreads();
    }
}

__global__ void update_weights_matrix(float* weight_matrix,
                                         float* raw_inputs,
                                         float* weighted_inputs,
                                         float* layer_errors,
                                         float learning_rate,
                                         int wmY, int wmX) {
        int threadX = threadIdx.x + blockIdx.x * blockDim.x;
        int threadY = threadIdx.y + blockIdx.y * blockDim.y;    // threadY is needed here, because we ARE operation on matrices during update phase

        int leX = wmX;  // layer_errors_X, this dimension matches the output dim of current weight matrix (X dim)
        int leY = 1;    // layer_errors_y = 1 because the input is a vector, and therefore all raw inputs are vectors

        int riX = wmY;  // raw_input_X, this dimension matches the input dim of current weights matrix (Y dim)
        int riY = 1;    // because the input is a vector, and all intermediate ones will be vectors too

        int cell_idx = threadY * wmX + threadX; // that seems correct

        // prepare the horizontal vector of the calculation
        if(threadX < leX && threadY == 0) {
            layer_errors[threadX] *= device_relu_derivative(weighted_inputs[threadX]) * learning_rate;
        }
        __syncthreads();

        // we're recreating the weights adjustment matrix from two vectors here,
        // so indices used to operate on both need to be distinctive (and I need to get them correctly)
        if ((threadX < wmX) && (threadY < wmY)) {
            // calculate the update values and immediately apply them to weights matrix
            weight_matrix[cell_idx] += device_gemm_single_cell_with_transposedA(raw_inputs, layer_errors, \
                                                                                  riX, leX, riY,                \
                                                                                  threadX, threadY);
        }
}