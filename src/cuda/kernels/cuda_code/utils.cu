__device__ void get_thread_idx(int* idx_out,
                               int block_dimX,
                               int block_dimY) {
    int threadX = threadIdx.x + blockIdx.x * blockDim.x;
    int threadY = threadIdx.y + blockIdx.y * blockDim.y;

    idx_out[0] = threadX;
    idx_out[1] = threadY;
}