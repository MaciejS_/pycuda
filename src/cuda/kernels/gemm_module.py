import numpy as np

import pycuda
import pycuda.gpuarray

from pycuda.gpuarray import GPUArray

from cuda.kernels.kernel_module import KernelModule
from cuda.kernels.nn_kernel import NNKernel
from cuda.kernels import KERNELS_ROOT


class GEMMModule(KernelModule):


    def __init__(self):
        super().__init__(r"%s\gemm.cu" % KERNELS_ROOT)

        self._gemm_kernel = self._pycuda_source_module.get_function('simple_gemm')
        self._gemm_transposedB_kernel = self._pycuda_source_module.get_function('gemm_with_transposedB')

    def ndarray_gemm(self, A: np.ndarray, B: np.ndarray, C: np.ndarray = None) -> (np.ndarray, None):
        """
        This is an alternative version of gpuarray_gemm that accepts numpy.ndarray matrices
        It is a bit slower since it must first copy the matrices to GPU

        The behaviour is identical to gpuarray_gemm, check out the docstring of gpuarray_gemm for more details.
        """
        gpu_a, gpu_b = map(pycuda.gpuarray.to_gpu, (A, B))
        gpu_c = pycuda.gpuarray.to_gpu(C) if C else None

        gpu_c = self.gpuarray_gemm(gpu_a, gpu_b, gpu_c)

        return gpu_c.get()


    def gpuarray_gemm(self, A: GPUArray, B: GPUArray, C: GPUArray = None) -> (GPUArray, None):
        """
        Perform a multiplication (dot product) of matrices A and B
        Matrix C is used to store the result, this will save time required for allocation.

        If argument matrices are not pycuda.GPUArray objects, an exception will be raised

        :param A: left operand of the dot product
        :param B: right operand of the dot product
        :param C: matrix in which result will be stored. If None, a new one will be created
        :return: matrix C is returned
        """
        for matrix in A, B:
            if len(matrix.shape) != 2:
                raise ValueError("GEMM operand must be a matrix")
            if matrix.dtype != np.float32:
                raise TypeError("GEMM operands must both contain np.float32 values")

        gemm_result_shape = self.gemm_result_shape(A, B)
        C = C or pycuda.gpuarray.zeros(gemm_result_shape, np.float32)

        heightA, widthA = map(np.int32, A.shape)
        heightB, widthB = map(np.int32, B.shape)

        # block size dimensions order: (X, Y, Z), numpy array holds (Y, X, Z)
        cuda_block_dim = KernelModule.block_dim_for_ndarray_shape(gemm_result_shape)

        self._gemm_kernel(A, B, C,
                          widthA, heightA,
                          widthB, heightB,
                          block=cuda_block_dim)

        return C

    def gpuarray_gemm_transposedB(self, A: GPUArray, B: GPUArray, C: GPUArray = None) -> (GPUArray, None):
        """
        Perform a multiplication (dot product) of matrices A and B
        Matrix C is used to store the result, this will save time required for allocation.

        If argument matrices are not pycuda.GPUArray objects, an exception will be raised

        :param A: left operand of the dot product
        :param B: right operand of the dot product
        :param C: matrix in which result will be stored. If None, a new one will be created
        :return: matrix C is returned
        """

        for matrix in A, B:
            # if len(matrix.shape) != 2:
            #     raise ValueError("GEMM operand must be a matrix")
            if matrix.dtype != np.float32:
                raise TypeError("GEMM operands must both contain np.float32 values")

        gemm_result_shape = A.shape[0], B.shape[0]
        C = C or pycuda.gpuarray.zeros(gemm_result_shape, np.float32)

        heightA, widthA = map(np.int32, NNKernel.shape_atleast2d(A))
        heightB, widthB = map(np.int32, NNKernel.shape_atleast2d(B))

        # block size dimensions order: (X, Y, Z), numpy array holds (Y, X, Z)
        cuda_block_dim = KernelModule.block_dim_for_ndarray_shape(gemm_result_shape)

        self._gemm_transposedB_kernel(A, B, C,
                                      widthA, heightA,
                                      widthB, heightB,
                                      block=cuda_block_dim)

        return C

    @staticmethod
    def gemm_result_shape(A: (np.ndarray, GPUArray), B: (np.ndarray, GPUArray)):
        """
        Returns a shape of the result of A.dot(B).
        The result shape complies to ndarray dimension order (Y, X, Z)

        :param A: left operand of GEMM
        :param B: right operand of GEMM
        :return: shape of the result of A.dot(B)
        """
        if len(A.shape) != 2 or len(B.shape) != 2:
            raise NotImplementedError("This method currently works only for 2D arrays")

        if A.shape[1] != B.shape[0]:
            raise ValueError("Incorrect dimensions for dot product: " + str(A.shape) + ", " + str(B.shape))

        return A.shape[0], B.shape[1]