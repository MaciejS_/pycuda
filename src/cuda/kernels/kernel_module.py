import numpy as np
import pycuda.compiler
import pycuda.gpuarray
from pycuda.gpuarray import GPUArray


class KernelModule:
    def __init__(self, kernel_module_file_path: str, include_dirs=None):
        """

        :param kernel_module_file_path: path to file that contains CUDA code
        """

        include_dirs = include_dirs or []
        with open(kernel_module_file_path) as kernel_code_file:
            kernel_code = kernel_code_file.read()
            self._pycuda_source_module = pycuda.compiler.SourceModule(kernel_code,
                                                                      include_dirs=include_dirs)

    @staticmethod
    def block_dim_for_ndarray(array: (np.ndarray, GPUArray)) -> tuple:
        """
        Returns a CUDA block dimensions corresponding to the shape of `array`, of equal size (resulting in one thread per cell of the original array)

        The reason why this function is used is that numpy.ndarray and CUDA block dims are not compatible
        Numpy ndarray dimensions are given in order specific to Linear Algebra convention: (Y, X, Z, N, M, ...)
        However, CUDA block size ordering is different: (X, Y, Z)

        :param array: array for which block dim will be returned
        :return: CUDA block dimensions for array
        """
        return KernelModule.block_dim_for_ndarray_shape(array.shape)

    @staticmethod
    def block_dim_for_ndarray_shape(shape: tuple):
        """
        Returns a CUDA block dimensions corresponding to the `shape`, of equal size (resulting in one thread per cell of the array described by `shape`)

        The reason why this function is used is that numpy.ndarray and CUDA block dims are not compatible
        Numpy ndarray dimensions are given in order specific to Linear Algebra convention: (Y, X, Z, N, M, ...)
        However, CUDA block size ordering is different: (X, Y, Z)

        :param array: array for which block dim will be returned
        :return: CUDA block dimensions for array
        """
        if sum(shape) > 0:
            if len(shape) == 1:
                if shape[0] > 0:
                    return shape + (1, 1)
            elif len(shape) == 2:
                return shape[::-1] + (1,)
            elif len(shape) == 3:
                return shape[1], shape[0], shape[2]

        raise ValueError("The array must be 1, 2 or 3-dimensional and not empty")

    @staticmethod
    def get_pointers_to_matrices(matrices: list):
        """
        Returns a vector of pointers, where each pointer points to on-device memory location where matrix data 'begins'
        :param matrices: list of matrices
        :return: list of pointers to on-device memory
        """
        for x in matrices:
            assert x.dtype == np.float32
            assert x.size == x.mem_size
        return pycuda.gpuarray.to_gpu(np.asarray([matrix.ptr for matrix in matrices], dtype=np.uint64))
