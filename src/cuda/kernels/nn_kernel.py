from cuda.kernels.kernel_module import KernelModule
from cuda.kernels import KERNELS_ROOT

import numpy as np
import pycuda.gpuarray
from pycuda.gpuarray import GPUArray


class NNKernel(KernelModule):
    def __init__(self):
        super().__init__(r"%s\nn.cu" % KERNELS_ROOT, include_dirs=[KERNELS_ROOT])
        self._feed_matrix = self._pycuda_source_module.get_function('feed_matrix')
        self._backpropagate_vector = self._pycuda_source_module.get_function('backpropagate_vector')
        self._update_weights_matrix = self._pycuda_source_module.get_function('update_weights_matrix')

    def feed_gpuarray(self,
                      input_matrix: GPUArray,
                      weight_matrices: list,
                      raw_input_matrices: list = None,
                      weighted_input_matrices: list = None) -> GPUArray:
        """

        :param input_matrix: a 2D matrix made up of input vectors
        :param weight_matrices: list of weight matrices
        :param raw_input_matrices: list of matrices that will hold the outputs of hidden neuron layers, these outputs can be used for backpropagation
        :return:
        """
        if not raw_input_matrices:
            raw_input_matrices = self.prepare_raw_input_matrices(input_matrix, weight_matrices)

        if not weighted_input_matrices:
            weighted_input_matrices = self.prepare_weighted_input_matrices(input_matrix, weight_matrices)

        weight_matrices_ptrs = self.get_pointers_to_matrices(weight_matrices)
        raw_input_matrices_ptrs = self.get_pointers_to_matrices(raw_input_matrices)
        weighted_input_matrices_ptrs = self.get_pointers_to_matrices(weighted_input_matrices)

        block_size = self.get_sufficient_block_size_for_forward_prop(input_matrix, weight_matrices)

        input_matrix_shape = NNKernel.shape_atleast2d(input_matrix)

        self._feed_matrix(weight_matrices_ptrs,
                          raw_input_matrices_ptrs,
                          weighted_input_matrices_ptrs,
                          np.int32(len(weight_matrices)),
                          self.get_weight_matrices_dims_as_gpu_vector(weight_matrices),
                          np.int32(input_matrix_shape[1]), np.int32(input_matrix_shape[0]),
                          block=block_size)

        output_matrix = raw_input_matrices[-1]
        return output_matrix

    def backpropagate_vector(self,
                             error_vector: GPUArray,
                             weighted_input_vectors: list,
                             raw_input_vectors: list,
                             weight_matrices: list,
                             intermediate_error_vectors: list = None,
                             learning_rate: float = 0.01
                             ) -> GPUArray:
        # TODO: optimization, can weighted input vectors replace intermediate error vectors?
        if not intermediate_error_vectors:
            intermediate_error_vectors = self.prepare_raw_input_matrices(error_vector, weight_matrices)
            intermediate_error_vectors[-1] = intermediate_error_vectors[0]
            intermediate_error_vectors = intermediate_error_vectors[1:]

        intermediate_error_vectors_ptrs = self.get_pointers_to_matrices(intermediate_error_vectors)
        weighted_input_vectors_ptrs = self.get_pointers_to_matrices(weighted_input_vectors)
        raw_input_vectors_ptrs = self.get_pointers_to_matrices(raw_input_vectors)
        weight_matrices_ptrs = self.get_pointers_to_matrices(weight_matrices)

        weight_matrices_dims = self.get_weight_matrices_dims_as_gpu_vector(weight_matrices)

        block_size = self.get_sufficient_block_size_for_backprop(weight_matrices)

        self._backpropagate_vector(weighted_input_vectors_ptrs,
                                   raw_input_vectors_ptrs,
                                   intermediate_error_vectors_ptrs,
                                   weight_matrices_ptrs,
                                   np.int32(len(weight_matrices)),
                                   weight_matrices_dims,
                                   np.float32(learning_rate),
                                   block=block_size)

        return weight_matrices

    def update_weights_matrix(self,
                              error_vector: GPUArray,
                              weighted_inputs_vector: GPUArray,
                              raw_input_vector: GPUArray,
                              weight_matrix: GPUArray,
                              learning_rate: float,
                              ) -> None:

        block_size = self.block_dim_for_ndarray_shape(weight_matrix.shape)

        self._update_weights_matrix(weight_matrix,
                                    raw_input_vector,
                                    weighted_inputs_vector,
                                    error_vector,
                                    np.float32(learning_rate),
                                    np.int32(block_size[1]), np.int32(block_size[0]),
                                    block=block_size)

    @staticmethod
    def prepare_raw_input_matrices(input_matrix: GPUArray, weight_matrices: list) -> list:
        """
        Creates a sequence of matrices that will hold the output of neuron layers
        The first matrix in sequence is the input matrix
        The last matrix is the output matrix
        Intermediate matrices are outputs of hidden layers
        :param input_matrix:
        :param weight_matrices:
        :return:
        """
        input_matrix_height = input_matrix.shape[0] if len(input_matrix.shape) == 2 else 1

        return [input_matrix.astype(np.float32)] \
               + [pycuda.gpuarray.zeros((input_matrix_height, matrix.shape[1]), dtype=np.float32)
                  for matrix
                  in weight_matrices]    \

    @staticmethod
    def prepare_weighted_input_matrices(input_matrix: GPUArray, weight_matrices: list) -> list:
        """
        Creates a sequence of matrices that will hold the activation values before apssing through the activation functions of neuron layers
        The last matrix is the output matrix
        Intermediate matrices are outputs of hidden layers
        :param input_matrix:
        :param weight_matrices:
        :return:
        """
        input_matrix_height = input_matrix.shape[0] if len(input_matrix.shape) == 2 else 1

        return [pycuda.gpuarray.zeros((input_matrix_height, matrix.shape[1]), dtype=np.float32)
                  for matrix
                  in weight_matrices]


    def test_ptr(self, vectors):

        gpu_vectors = map(pycuda.gpuarray.to_gpu, vectors)
        vector_lengths = np.asarray(list(map(len, vectors)), dtype=np.int32)
        xdim = int(max(vector_lengths))
        ydim = len(vector_lengths)

        gpu_vector_lengths = pycuda.gpuarray.to_gpu(vector_lengths)
        gpu_vector_ptrs = self.get_pointers_to_matrices(gpu_vectors)

        self._ptr_test(gpu_vector_ptrs, gpu_vector_lengths, np.int32(len(vectors)), block=(xdim, ydim, 1))

        return [vec.get() for vec in gpu_vectors]

    @staticmethod
    def get_weight_matrices_dims_as_gpu_vector(weight_matrices) -> GPUArray:
        if not all((len(x.shape) == 2 for x in weight_matrices)):
            raise ValueError("All weight matrices must be 2 dimensional")

        dims_ndarray = np.concatenate([x.shape for x in weight_matrices]).astype(np.int32)
        return pycuda.gpuarray.to_gpu(dims_ndarray)

    @staticmethod
    def get_sufficient_block_size_for_forward_prop(input_matrix: (np.ndarray, GPUArray), weight_matrices: list) -> tuple:
        """
        Returns a block size that will suffice for any computation in this neural network, given the size of input.
        Internally this function takes the maximum X of weight matrices and Y of input matrix
        As a result, every GEMM result matrix will fit into such a block
        :param input_matrix: matrix containing neural network inputs
        :param weight_matrices: iterable of weight matrices, may either be np.ndarray or GPUArray
        :return: tuple
        """
        max_x = max(matrix.shape[1] for matrix in weight_matrices)
        max_y = input_matrix.shape[0] if len(input_matrix.shape) == 2 else 1 # y dim of result matrices in NN is always determined by input matrix
        return tuple(map(int, (max_x, max_y, 1)))

    @staticmethod
    def get_sufficient_block_size_for_backprop(weight_matrices: list) -> tuple:
        """
        Returns a block size that will suffice for any computation during backprop, given the size of weight matrices.
        Internally this function takes the maximum Y and X dimensions of weight matrices and returns them as a tuple
        Backprop is carried out one vector at a time (to guarantee determinism, also batched mode is not implemented)
        As a result, every weight adjustment matrix will fit into such a block

        :param weight_matrices: iterable of weight matrices, may either be np.ndarray or GPUArray
        :return: tuple
        """
        max_y, max_x = np.max(np.vstack(x.shape for x in weight_matrices), axis=0)
        return tuple(map(int, (max_x, max_y, 1)))

    @staticmethod
    def shape_atleast2d(array):
        """
        Return shape of this array as if it were at least 2-dimensional
        :param array:
        :return:
        """
        return array.shape if len(array.shape) == 2 else (1, array.shape[0])