from cuda.neural_network.cuda_nn import MonolithicCUDANN
from cuda.neural_network.cuda_nn import ModularCUDANN

from datetime import datetime as dt
from datetime import timedelta as td
import numpy as np

import matplotlib.pyplot as plt

from cuda.showcase.performance import print_results

input_layer_size = 10
hidden_layer_size = 5
output_layer_size = 2


def get_fwdprop_time(input_layer_size,
                     hidden_layer_sizes,
                     output_layer_size,
                     repeats=1):
    inputs = np.atleast_2d(np.random.rand(input_layer_size))

    layer_sizes = list(hidden_layer_sizes) + [output_layer_size]

    weight_matrices = MonolithicCUDANN.generate_random_weights(input_layer_size,
                                                               layer_sizes)

    monolithic_cuda_nn = MonolithicCUDANN(input_layer_size,
                                          layer_sizes,
                                          weight_matrices)
    modular_cuda_nn = ModularCUDANN(input_layer_size,
                                    layer_sizes,
                                    weight_matrices)

    results = {"tested_networks": ("modular", "monolithic"),
               "size_info": [input_layer_size] + layer_sizes,
               "attempts": repeats,
               "modular": [],
               "monolithic": []}

    for i in range(repeats):
        for name, nn in {"modular": modular_cuda_nn, "monolithic": monolithic_cuda_nn}.items():
            t_start = dt.now()
            nn.feed(inputs)
            t_end = dt.now()
            time_elapsed = t_end - t_start
            results[name].append(time_elapsed.total_seconds())

    return results


def plot_results(result_data, title):
    tested_networks = result_data[0]["tested_networks"]
    x_axis = [sum(single_test_result["size_info"]) for single_test_result in result_data]
    y_axes = {name: [np.average(single_test_result[name]) for single_test_result in result_data]
              for name
              in tested_networks}

    colors = ['r', 'g', 'b']
    series = []
    for name, data_points in y_axes.items():
        series.append(plt.plot(x_axis, data_points, colors.pop(0), label=name)[0])

    plt.xlabel("number of neurons in network")
    plt.ylabel("time [s]")
    plt.title(title)
    plt.legend(handles=series)
    plt.show()




print_results(get_fwdprop_time(10, (5,), 2, repeats=100), )
print_results(get_fwdprop_time(1000, (500, 200, 100), 50, repeats=100), )
print_results(get_fwdprop_time(1000, np.random.randint(low=100, high=1000, size=30), 50, repeats=100))

for x in range(1, 32):
    print_results(get_fwdprop_time(256, (256,) * x, 256, repeats=10), )


