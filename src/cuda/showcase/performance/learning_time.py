from cuda.neural_network.learning_cuda_nn import MonolithicLearningCUDANN
from cuda.neural_network.learning_cuda_nn import ModularLearningCUDANN

from datetime import datetime as dt
from datetime import timedelta as td
import numpy as np

from cuda.showcase.performance import print_results

import pycuda.autoinit
import pycuda.gpuarray

import matplotlib.pyplot as plt

def compare_modular_and_monolithic_learning_time(input_layer_size,
                                                 hidden_layer_sizes,
                                                 output_layer_size,
                                                 examples_set_size=100):
    inputs = np.atleast_2d(np.random.rand(input_layer_size))

    layer_sizes = list(hidden_layer_sizes) + [output_layer_size]

    weight_matrices = MonolithicLearningCUDANN.generate_random_weights(input_layer_size,
                                                                       layer_sizes)

    monolithic_nn = MonolithicLearningCUDANN(input_layer_size, layer_sizes, weight_matrices)
    modular_nn = ModularLearningCUDANN(input_layer_size, layer_sizes, weight_matrices, activation_functions=(None,) * len(layer_sizes))

    results = {"tested_networks": ("modular", "monolithic"),
               "training_set_size": examples_set_size,
               "size_info": [input_layer_size] + layer_sizes,
               "modular": [],
               "monolithic": []}

    training_examples = [pycuda.gpuarray.to_gpu(np.random.rand(*inputs.shape)).astype(np.float32) for _ in
                         range(examples_set_size)]
    expected_outputs = [pycuda.gpuarray.to_gpu(np.atleast_2d(np.random.rand(output_layer_size))).astype(np.float32) for
                        _ in range(examples_set_size)]

    for name, nn in {"modular": modular_nn, "monolithic": monolithic_nn}.items():
        t_start = dt.now()
        nn.learnOn(training_examples, expected_outputs)
        t_end = dt.now()
        time_elapsed = t_end - t_start
        results[name].append(time_elapsed.total_seconds())

    return results


def time_benefit_when_reusing_gpu_memory(input_layer_size,
                                         hidden_layer_sizes,
                                         output_layer_size,
                                         examples_set_size=100):
    inputs = np.atleast_2d(np.random.rand(input_layer_size))

    layer_sizes = list(hidden_layer_sizes) + [output_layer_size]

    weight_matrices = MonolithicLearningCUDANN.generate_random_weights(input_layer_size,
                                                                       layer_sizes)

    reusing_nn = MonolithicLearningCUDANN(input_layer_size, layer_sizes, weight_matrices, reuse_memory=True)
    non_reusing_nn = MonolithicLearningCUDANN(input_layer_size, layer_sizes, weight_matrices, reuse_memory=False)

    results = {"tested_networks": ("reusing", "non-reusing"),
               "training_set_size": examples_set_size,
               "size_info": [input_layer_size] + layer_sizes,
               "reusing": [],
               "non-reusing": []}

    training_examples = [pycuda.gpuarray.to_gpu(np.random.rand(*inputs.shape)).astype(np.float32)
                         for _
                         in range(examples_set_size)]
    expected_outputs = [pycuda.gpuarray.to_gpu(np.atleast_2d(np.random.rand(output_layer_size))).astype(np.float32)
                        for _
                        in range(examples_set_size)]

    for name, nn in {"reusing": reusing_nn, "non-reusing": non_reusing_nn}.items():
        t_start = dt.now()
        nn.learnOn(training_examples, expected_outputs)
        t_end = dt.now()
        time_elapsed = t_end - t_start
        results[name].append(time_elapsed.total_seconds())

    return results


def print_results(result_data):
    averages = {}

    print("{n} examples {n_layers} layers, {n_neurons} neurons total".format(n=result_data["training_set_size"],
                                                                             n_layers=len(result_data["size_info"]),
                                                                             n_neurons=sum(result_data["size_info"])))


    for nn_name in result_data["tested_networks"]:
        nn_times = result_data[nn_name]
        average_time = sum(nn_times) / len(nn_times)
        averages[nn_name] = average_time
        print("{name}: {time:3f}".format(name=nn_name,
                                                                        time=average_time))

    sorted_times = sorted(averages.items(), key=lambda x: x[1])
    time_diff = abs(sorted_times[0][1] - sorted_times[1][1])
    time_ratio = sorted_times[0][1] / sorted_times[1][1]
    print("On average, {name} was {value:3f} faster ({percentage}%)".format(name=sorted_times[0][0],
                                                                            value=time_diff,
                                                                            percentage=int((1 - time_ratio) * 100)))
    print()

def plot_results(result_data, title):
    tested_networks = result_data[0]["tested_networks"]
    x_axis = [sum(single_test_result["size_info"]) for single_test_result in result_data]
    y_axes = {name: [single_test_result[name] for single_test_result in result_data]
              for name
              in tested_networks}

    colors = ['r', 'g', 'b']
    series = []
    for name, data_points in y_axes.items():
        series.append(plt.plot(x_axis, data_points, colors.pop(0), label=name)[0])

    plt.xlabel("number of neurons in network")
    plt.ylabel("time [s]")
    plt.title(title)
    plt.legend(handles=series)
    plt.show()



n_neurons = 32

mod_mon_results = []
max_layers = 32
for x in range(1, max_layers):
    mod_mon_results.append(compare_modular_and_monolithic_learning_time(n_neurons, (n_neurons,) * x, n_neurons))
    print_results(mod_mon_results[-1])

reuse_results = []
for x in range(1, max_layers):
    reuse_results.append(time_benefit_when_reusing_gpu_memory(n_neurons, (n_neurons,) * x, n_neurons))
    print_results(reuse_results[-1])

plot_results(mod_mon_results, "Modular vs. monolithic")
plot_results(reuse_results, "Monolithic with memory reuse vs. no memory reuse")

