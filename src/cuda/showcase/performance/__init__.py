
def print_results(result_data):
    averages = {}

    print("{n} attempts {n_layers} layers, {n_neurons} neurons total".format(n=result_data["attempts"],
                                                                             n_layers=len(result_data["size_info"]),
                                                                             n_neurons=sum(result_data["size_info"])))
    for nn_name in result_data["tested_networks"]:
        nn_times = result_data[nn_name]
        average_time = sum(nn_times) / len(nn_times)
        averages[nn_name] = average_time
        print("{name}: min {min:3f}, avg {avg:3f}, max {max:3f}".format(name=nn_name,
                                                                        min=min(nn_times),
                                                                        avg=average_time,
                                                                        max=max(nn_times)))

    sorted_times = sorted(averages.items(), key=lambda x: x[1])
    time_diff = abs(sorted_times[0][1] - sorted_times[1][1])
    time_ratio = sorted_times[0][1] / sorted_times[1][1]
    print("On average, {name} was {value:3f} faster ({percentage}%)".format(name=sorted_times[0][0],
                                                                            value=time_diff,
                                                                            percentage=int((1 - time_ratio) * 100)))
    print()
