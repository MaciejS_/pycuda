from cuda.kernels.gemm_module import GEMMModule
from cuda.kernels.nn_kernel import NNKernel
from cuda.kernels.relu_kernel import ReLUKernel
from cuda.neural_network.cuda_nn import ModularCUDANN, MonolithicCUDANN
from nn.neural_network.network.learning_network import LearningNN

import pycuda.autoinit
import pycuda.gpuarray
from pycuda.gpuarray import GPUArray

import numpy as np

class LearningCUDANN(LearningNN, ModularCUDANN):
    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple = None,
                 learning_rate = 0.01):
        # raise NotImplementedError("Check out how to implement multiple inheritance correctly")
        super(ModularCUDANN, self).__init__(inputs,
                         layer_sizes,
                         weight_matrices,
                         activation_functions)

    @classmethod
    def from_weight_matrices(cls,
                             weight_matrices: tuple,
                             activation_functions: tuple = None,
                             learning_rate: float = 0.03):
        instance = super().from_weight_matrices(weight_matrices, activation_functions)
        instance._learning_rate = learning_rate
        return instance

    def feed(self, inputs: GPUArray):
        raise NotImplementedError

    def backpropagate(self, errors):
        raise NotImplementedError


class ModularLearningCUDANN(LearningCUDANN, ModularCUDANN):

    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple = None,
                 learning_rate=0.01):
        super().__init__(inputs,
                                            layer_sizes,
                                            weight_matrices,
                                            activation_functions,
                                            learning_rate)
        self.nn_kernel = NNKernel()
        self.gemm_kernel = GEMMModule()
        self.relu_kernel = ReLUKernel()
        self._learning_rate = learning_rate

        self._gpu_weight_matrices = [x.astype(np.float32) for x in map(pycuda.gpuarray.to_gpu, self._layer_weights)]


    def feed(self, inputs: GPUArray):
        # TODO: this implementation does a lot of ad-hoc allocations, optimize by pre-allocating and reusing

        if len(inputs.shape) == 1:
            inputs = inputs.reshape(1, inputs.shape[0])

        next_layer_inputs = inputs.astype(np.float32)

        self._gpu_raw_input_matrices = [next_layer_inputs]
        self._gpu_weighted_input_matrices = []

        # call cuda GEMM on consecutive weight matrices
        for weight_matrix in self._gpu_weight_matrices:
            gemm_result = self.gemm_kernel.gpuarray_gemm(next_layer_inputs, weight_matrix)
            self._gpu_weighted_input_matrices.append(gemm_result)

            next_layer_inputs = self.relu_kernel.apply(gemm_result)
            self._gpu_raw_input_matrices.append(next_layer_inputs)

        return next_layer_inputs

    def learnOn(self, training_examples, expected_outputs):
        super().learnOn(training_examples, expected_outputs)

    def backpropagate(self, errors: GPUArray):
        errors_on_gpu = pycuda.gpuarray.to_gpu(errors).astype(np.float32) if not isinstance(errors, GPUArray) else errors

        # intermediate_layer_errors = [pycuda.gpuarray.zeros((1, weights_matrix.shape[1]), dtype=np.float32)
        #                              for weights_matrix
        #                              in self.weight_matrices[1:-1]] + [errors_on_gpu]

        intermediate_layer_errors = [errors_on_gpu]
        errors_vector = errors_on_gpu
        for weights_matrix in reversed(self._gpu_weight_matrices[1:]):
            previous_layer_errors_vector = self.gemm_kernel.gpuarray_gemm_transposedB(errors_vector, weights_matrix)
            intermediate_layer_errors.insert(0, previous_layer_errors_vector)
            errors_vector = previous_layer_errors_vector

        for x, h, e, wm in zip(self._gpu_raw_input_matrices,
                           self._gpu_weighted_input_matrices,
                           intermediate_layer_errors,
                           self._gpu_weight_matrices):
            self.nn_kernel.update_weights_matrix(e, h, x, wm, self._learning_rate)



class MonolithicLearningCUDANN(LearningCUDANN, MonolithicCUDANN):

    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple = None,
                 learning_rate = 0.01,
                 reuse_memory: bool = True):
        super(MonolithicCUDANN, self).__init__(inputs,
                         layer_sizes,
                         weight_matrices,
                         activation_functions)

        self.nn_kernel = NNKernel()
        self._learning_rate = learning_rate
        self._previous_input_shape = (0, 0)
        self.reuse_memory = reuse_memory


    def feed(self, inputs: GPUArray) -> GPUArray:
        """
        Feeds inputs array (vector or matrix) into the neural network and returns the predictions

        For maximum performance avoid changing the Y dimension of inputs (changes of Y dim require the intermediate matrices to be reallocated)
        :param inputs:
        :return:
        """
        inputs_on_gpu = pycuda.gpuarray.to_gpu(inputs) if isinstance(inputs, np.ndarray) else inputs
        current_inputs_shape = NNKernel.shape_atleast2d(inputs)

        if current_inputs_shape[0] != self._previous_input_shape[0] or not self.reuse_memory:
            self._gpu_raw_input_matrices = self.nn_kernel.prepare_raw_input_matrices(inputs_on_gpu, self.weight_matrices)
            self._gpu_weighted_input_matrices = self.nn_kernel.prepare_weighted_input_matrices(inputs_on_gpu, self.weight_matrices)
            self._previous_input_shape = current_inputs_shape

        self._gpu_raw_input_matrices[0] = inputs_on_gpu.astype(np.float32)

        output = self.nn_kernel.feed_gpuarray(inputs_on_gpu,
                                              self._gpu_weight_matrices,
                                              self._gpu_raw_input_matrices,
                                              self._gpu_weighted_input_matrices)

        return output

    def learnOn(self, training_examples, expected_outputs):
        # can CUDANN be expected to handle type conversions so that when feed is called implicitly, the arguments will be valid GPUArrays?
        super().learnOn(training_examples, expected_outputs)
        # raise NotImplementedError

    def backpropagate(self, errors):
        if len(errors.shape) > 2:
            raise ValueError("A matrix has only 2 dimensions, you know")

        for error_vector in errors:
            self.nn_kernel.backpropagate_vector(error_vector,
                                                self._gpu_weighted_input_matrices,
                                                self._gpu_raw_input_matrices,
                                                self._gpu_weight_matrices,
                                                learning_rate=self._learning_rate)