import logging

import numpy as np
import pycuda.autoinit
import pycuda.compiler
import pycuda.gpuarray
from pycuda.gpuarray import GPUArray

from cuda.kernels.gemm_module import GEMMModule
from cuda.kernels.nn_kernel import NNKernel
from cuda.kernels.relu_kernel import ReLUKernel
from nn.neural_network.network.network import NeuralNetwork

logging.basicConfig()

class ModularCUDANN(NeuralNetwork):

    # TODO need to handle the activation functions (

    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple=None):
        super().__init__(inputs,
                         layer_sizes,
                         weight_matrices,
                         (None, )*len(weight_matrices))

        self.relu_kernel = ReLUKernel()
        self.gemm_kernel = GEMMModule()

        self._gpu_weight_matrices = [x.astype(np.float32) for x in map(pycuda.gpuarray.to_gpu, self._layer_weights)]

    def feed(self, inputs: np.ndarray) -> np.ndarray:

        if len(inputs.shape) < 2:
            inputs = np.atleast_2d(inputs)
        next_layer_inputs = pycuda.gpuarray.to_gpu(inputs.astype(np.float32))

        # call cuda GEMM on consecutive weight matrices
        for weight_matrix in self._gpu_weight_matrices:
            gemm_result = self.gemm_kernel.gpuarray_gemm(next_layer_inputs, weight_matrix)
            next_layer_inputs = self.relu_kernel.apply(gemm_result)

        return next_layer_inputs.get()

    @classmethod
    def from_size_data(cls,
                       inputs: int,
                       layer_sizes: tuple,
                       activation_functions: tuple = None,
                       weights_range: tuple = (-1, 1)):

        return super().from_size_data(inputs,
                                      layer_sizes,
                                      (None,) * len(layer_sizes),
                                      weights_range)

    @classmethod
    def from_weight_matrices(cls,
                             weight_matrices: tuple,
                             activation_functions: tuple = None):

        instance = super().from_weight_matrices(weight_matrices, (None,) * len(weight_matrices))
        instance._gpu_weight_matrices = [x.astype(np.float32) for x in map(pycuda.gpuarray.to_gpu, instance._layer_weights)]

        return instance


    @property
    def weight_matrices(self) -> list:
        """
        Returns the ndarray weight matrices used by neuron layers of this network
        :return: tuple of np.ndarray
        """
        return [gpu_weight_matrix.get() for gpu_weight_matrix in self._gpu_weight_matrices]

    @weight_matrices.setter
    def weight_matrices(self, weight_matrices: tuple) -> None:
        """
        Updates weight matrices of internal neuron layers with matrices in `weight_matrices`
        ValueError will be raised if number of passed matrices is not equal to the number of neuron layers in this network

        :param weight_matrices: tuple of np.ndarray
        :return: None
        """
        if weight_matrices is not None and len(weight_matrices) == len(self._layer_weights):
            if all(new_matrix.shape == old_matrix.shape for new_matrix, old_matrix in zip(self._layer_weights, weight_matrices)):
                self._layer_weights = weight_matrices
                self._gpu_weight_matrices = map(pycuda.gpuarray.to_gpu, self._layer_weights)
            else:
                raise ValueError("New weight matrices are misshaped")
        else:
            raise ValueError("Failed to update weight matrices: %d layers but %d matrices" %
                             (len(self._layer_weights), len(weight_matrices)))


class MonolithicCUDANN(ModularCUDANN):
    """
    This ModularCUDANN implementation uses the monolithic NNKernel instead of GEMMKernel
    """

    def __init__(self,
                 inputs: int,
                 layer_sizes: tuple,
                 weight_matrices: tuple,
                 activation_functions: tuple = None,
                 reuse_memory: bool = True):
        super().__init__(inputs,
                         layer_sizes,
                         weight_matrices,
                         (None, )*len(weight_matrices))

        self.nn_kernel = NNKernel()
        self._gpu_weight_matrices = list(map(pycuda.gpuarray.to_gpu, self._layer_weights))
        self._previous_input_shape = (0, 0)
        self.reuse_memory = reuse_memory

        self._intermediate_matrices = None

    def feed(self, inputs: np.ndarray) -> np.ndarray:
        inputs_on_gpu = pycuda.gpuarray.to_gpu(inputs) if not isinstance(inputs, GPUArray) else inputs
        inputs_on_gpu = inputs_on_gpu.astype(np.float32)

        current_inputs_shape = NNKernel.shape_atleast2d(inputs_on_gpu)
        if current_inputs_shape[0] != self._previous_input_shape[0] or not self.reuse_memory:
            self._intermediate_matrices = self.nn_kernel.prepare_raw_input_matrices(inputs_on_gpu,
                                                                              self._gpu_weight_matrices)
            self._previous_input_shape = current_inputs_shape

        self._intermediate_matrices[0] = inputs_on_gpu
        output = self.nn_kernel.feed_gpuarray(inputs_on_gpu,
                                              self._gpu_weight_matrices,
                                              self._intermediate_matrices)

        return output.get()
